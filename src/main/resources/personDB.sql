INSERT INTO peopledb.person(imie, nazwisko, plec, PESEL, data_urodzenia, obywatelstwo, email, telefon_stacjonarny, telefon_komorkowy, kraj, miasto, ulica, nr_budynku, kod_pocztowy)
VALUES ("Jan", "Nowak", "M", "76101225699", "1976-10-12", "polskie", "jnowak@gmail.com", null, "+48111111111", "Polska", "Łódź", "Piotrowska", "58", "90-105"),
("Joanna", "Dudzik", "K", "90030136785", "1990-03-01", "polskie", "joana90@o2.pl", null, "+48222222222", "Polska", "Warszawa", "Domaniewska", "6", "02-663"),
("Agata", "Zięba", "K", "72061916745", "1972-06-19", "polskie", "sloneczko72@op.pl", "333333333", "+48333333333", "Polska", "Katowice", "Mickiewicza", "25", "40-085"),
("Dominik", "Rostkiewicz", "M", "85090846918", "1985-09-08", "polskie", "rumcajs@interia.eu", null, "+48444444444", "Polska", "Chorzów", "Wolności", "1", "41-500"),
("Piotr", "Maniek", "M", "55101486178", "1955-10-14", "polskie", "darkwarrior@o2.pl", null, "+48555555555", "Polska", "Gliwice", "Chorzowska", "50", "44-100"),
("Katarzyna", "Burza", "K", "40051864169", "1940-05-18", "polskie", null, null, "+48666666666", "Polska", "Czechowice - Dziedzice", "Bajeczna", "1", "43-502"),
("Marek", "Armata", "M", "61020513196", "1961-02-05", "polskie", null, null, "+48777777777", "Polska", "Polanica Zdrój", "Warszawska", "10", "57-320"),
("Ania", "Romańczyk", "K", "01280326141", "2001-08-03", "polskie", "ann_romanczyk@gmail.com", null, "+48888888888", "Polska", "Wrocław", "Myśliwska", "83", "50-241"),
("Magdalena", "Storczyk", "K", "85112456183", "1985-11-24", "polskie", "irongirl85@o2.pl", null, "+48999999999", "Polska", "Lublin", "Dobra", "31", "20-350"),
("Tomasz", "Kasa", "M", "89021735719", "1989-02-17", "polskie", null, null, "+48211111111", "Polska", "Lublin", "Dobra", "31", "20-350"),
("Radosław", "Szatan", "M", "78070725679", "1978-07-08", "polskie", "radosnyszatan78@op.pl", null, "+48222222222", "Polska", "Olsztyn", "Żołnierska", "47", "10-560");

