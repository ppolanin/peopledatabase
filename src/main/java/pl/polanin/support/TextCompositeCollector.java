package pl.polanin.support;

import javafx.scene.control.TextField;
import javafx.scene.control.Label;

public class TextCompositeCollector {

    private final String id;
    private final TextField textField;
    private final Label label;
    private final boolean isRequired;

    public TextCompositeCollector(String id, TextField component, boolean isRequired) {
        this(id, component, null, isRequired);
    }

    public TextCompositeCollector(String id, Label label, boolean isRequired) {
        this(id, null, label, isRequired);
    }

    public TextCompositeCollector(String id, TextField component, Label label, boolean isRequired) {
        this.id = id;
        this.textField = component;
        this.label = label;
        this.isRequired = isRequired;
    }

    public String getId() {
        return id;
    }

    public TextField getTextField() {
        return textField;
    }

    public Label getLabel() {
        return label;
    }

    public boolean isRequired() {
        return isRequired;
    }
}
