package pl.polanin.support;

import com.itextpdf.kernel.geom.PageSize;

public class PdfDocumentConfiguration {

    private final String documentHeader;
    private final String headerFont;
    private final float headerFontSize;
    private final String documentFont;
    private final float documentFontSize;
    private final PageSize documentPageSize;
    private final boolean isPageRotated;
    private final float documentTopMargin;
    private final float documentRightMargin;
    private final float documentLeftMargin;
    private final float documentBottomMargin;

    private PdfDocumentConfiguration(PdfDocumentConfigurationBuilder builder) {
        this.documentHeader = builder.documentHeader;
        this.headerFont = builder.headerFont;
        this.headerFontSize = builder.headerFontSize;
        this.documentFont = builder.documentFont;
        this.documentFontSize = builder.documentFontSize;
        this.documentPageSize = builder.documentPageSize;
        this.isPageRotated = builder.isPageRotated;
        this.documentTopMargin = builder.documentTopMargin;
        this.documentRightMargin = builder.documentRightMargin;
        this.documentLeftMargin = builder.documentLeftMargin;
        this.documentBottomMargin = builder.documentBottomMargin;
    }

    public String getDocumentHeader() {
        return documentHeader;
    }

    public String getHeaderFont() {
        return headerFont;
    }

    public float getHeaderFontSize() {
        return headerFontSize;
    }

    public String getDocumentFont() {
        return documentFont;
    }

    public float getDocumentFontSize() {
        return documentFontSize;
    }

    public PageSize getDocumentPageSize() {
        return documentPageSize;
    }

    public boolean isPageRotated() {
        return isPageRotated;
    }

    public float getDocumentTopMargin() {
        return documentTopMargin;
    }

    public float getDocumentRightMargin() {
        return documentRightMargin;
    }

    public float getDocumentLeftMargin() {
        return documentLeftMargin;
    }

    public float getDocumentBottomMargin() {
        return documentBottomMargin;
    }

    @Override
    public String toString() {
        return "PdfDocumentConfiguration{" +
                "documentHeader='" + documentHeader + '\'' +
                ", headerFont='" + headerFont + '\'' +
                ", headerFontSize=" + headerFontSize +
                ", documentFont='" + documentFont + '\'' +
                ", documentFontSize=" + documentFontSize +
                ", documentPageSize=" + documentPageSize +
                ", isPageRotated=" + isPageRotated +
                ", documentTopMargin=" + documentTopMargin +
                ", documentRightMargin=" + documentRightMargin +
                ", documentLeftMargin=" + documentLeftMargin +
                ", documentBottomMargin=" + documentBottomMargin +
                '}';
    }

    public static class PdfDocumentConfigurationBuilder {
        private String documentHeader;
        private String headerFont;
        private float headerFontSize;
        private String documentFont;
        private float documentFontSize;
        private PageSize documentPageSize;
        private boolean isPageRotated;
        private float documentTopMargin;
        private float documentRightMargin;
        private float documentLeftMargin;
        private float documentBottomMargin;

        public PdfDocumentConfigurationBuilder setDocumentHeader(String documentHeader) {
            this.documentHeader = documentHeader;
            return this;
        }

        public PdfDocumentConfigurationBuilder setHeaderFont(String headerFont) {
            this.headerFont = headerFont;
            return this;
        }

        public PdfDocumentConfigurationBuilder setHeaderFontSize(float headerFontSize) {
            this.headerFontSize = headerFontSize;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentFont(String documentFont) {
            this.documentFont = documentFont;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentFontSize(float documentFontSize) {
            this.documentFontSize = documentFontSize;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentPageSize(PageSize documentPageSize) {
            this.documentPageSize = documentPageSize;
            return this;
        }

        public PdfDocumentConfigurationBuilder isPageRotated(boolean pageRotated) {
            isPageRotated = pageRotated;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentTopMargin(float documentTopMargin) {
            this.documentTopMargin = documentTopMargin;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentRightMargin(float documentRightMargin) {
            this.documentRightMargin = documentRightMargin;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentLeftMargin(float documentLeftMargin) {
            this.documentLeftMargin = documentLeftMargin;
            return this;
        }

        public PdfDocumentConfigurationBuilder setDocumentBottomMargin(float documentBottomMargin) {
            this.documentBottomMargin = documentBottomMargin;
            return this;
        }

        public PdfDocumentConfiguration build() {
            return new PdfDocumentConfiguration(this);
        }
    }

}
