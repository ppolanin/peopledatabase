package pl.polanin.support;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataConverter {

    private DataConverter() {
    }

    public static LocalDate convertDateToSQLDateFormat(String date) {
        if (date != null) {
            String dateToConvert = date.replace(".", "-");
            Pattern datePattern1 = Pattern.compile("(\\d\\d)-(\\d\\d)-(\\d\\d\\d\\d)");
            Pattern datePattern2 = Pattern.compile("(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)");
            Matcher dateMatcher1 = datePattern1.matcher(dateToConvert);
            Matcher dateMatcher2 = datePattern2.matcher(dateToConvert);
            if (dateMatcher1.matches()) {
                return LocalDate.of(Integer.parseInt(dateMatcher1.group(3)), Integer.parseInt(dateMatcher1.group(2)), Integer.parseInt(dateMatcher1.group(1)));
            } else if (dateMatcher2.matches()) {
                return LocalDate.of(Integer.parseInt(dateMatcher2.group(1)), Integer.parseInt(dateMatcher2.group(2)), Integer.parseInt(dateMatcher2.group(3)));
            }
        }
        return null;
    }

    public static String convertSQLDateToDotDateFormat(String date) {
        if (date != null) {
            String dateToConvert = date.replace("-", ".");
            Pattern datePattern = Pattern.compile("(\\d\\d\\d\\d).(\\d\\d).(\\d\\d)");
            Matcher dateMatcher = datePattern.matcher(dateToConvert);
            if (dateMatcher.matches()) {
                return dateMatcher.group(3) + "." + dateMatcher.group(2) + "." + dateMatcher.group(1);
            }
        }
        return null;
    }
}