package pl.polanin.support;

import java.util.HashMap;
import java.util.Map;

public class ReferencedFont {

    private static final Map<String, String> referencedFonts = new HashMap<>();
    public static final String ARIAL_REGULAR_FONT = "Arial";
    public static final String ARIAL_BOLD_FONT = "Arial-Bold";
    public static final String ARIAL_ITALIC_FONT = "Arial-Italic";
    public static final String ARIAL_BOLD_ITALIC_FONT = "Arial-BoldItalic";
    public static final String ARIAL_NARROW_REGULAR_FONT = "Arial Narrow";
    public static final String ARIAL_NARROW_BOLD_FONT = "Arial Narrow-Bold";
    public static final String ARIAL_NARROW_ITALIC_FONT = "Arial Narrow-Italic";
    public static final String ARIAL_NARROW_BOLD_ITALIC_FONT = "Arial Narrow-BoldItalic";
    public static final String ARIAL_BLACK_FONT = "Arial Black";
    public static final String TIMES_NEW_ROMAN_REGULAR_FONT = "Times New Roman";
    public static final String TIMES_NEW_ROMAN_BOLD_FONT = "Times New Roman-Bold";
    public static final String TIMES_NEW_ROMAN_ITALIC_FONT = "Times New Roman-Italic";
    public static final String TIMES_NEW_ROMAN_BOLD_ITALIC_FONT = "Times New Roman-BoldItalic";
    public static final String VERDANA_REGULAR_FONT = "Verdana";
    public static final String VERDANA_BOLD_FONT = "Verdana-Bold";
    public static final String VERDANA_ITALIC_FONT = "Verdana-Italic";
    public static final String VERDANA_BOLD_ITALIC_FONT = "Verdana-BoldItalic";

    private ReferencedFont() {
    }

    static {
        referencedFonts.put(ARIAL_REGULAR_FONT, "FONTS/arial.ttf");
        referencedFonts.put(ARIAL_BOLD_FONT, "FONTS/arialbd.ttf");
        referencedFonts.put(ARIAL_BOLD_ITALIC_FONT, "FONTS/arialbi.ttf");
        referencedFonts.put(ARIAL_ITALIC_FONT, "FONTS/ariali.ttf");
        referencedFonts.put(ARIAL_NARROW_REGULAR_FONT, "FONTS/ARIALN.TTF");
        referencedFonts.put(ARIAL_NARROW_BOLD_FONT, "FONTS/ARIALNB.TTF");
        referencedFonts.put(ARIAL_NARROW_BOLD_ITALIC_FONT, "FONTS/ARIALNBI.TTF");
        referencedFonts.put(ARIAL_NARROW_ITALIC_FONT, "FONTS/ARIALNI.TTF");
        referencedFonts.put(ARIAL_BLACK_FONT, "FONTS/ariblk.ttf");
        referencedFonts.put(TIMES_NEW_ROMAN_REGULAR_FONT, "FONTS/times.ttf");
        referencedFonts.put(TIMES_NEW_ROMAN_BOLD_FONT, "FONTS/timesbd.ttf");
        referencedFonts.put(TIMES_NEW_ROMAN_ITALIC_FONT, "FONTS/timesi.ttf");
        referencedFonts.put(TIMES_NEW_ROMAN_BOLD_ITALIC_FONT, "FONTS/timesbi.ttf");
        referencedFonts.put(VERDANA_REGULAR_FONT, "FONTS/verdana.ttf");
        referencedFonts.put(VERDANA_BOLD_FONT, "FONTS/verdanab.ttf");
        referencedFonts.put(VERDANA_ITALIC_FONT, "FONTS/verdanai.ttf");
        referencedFonts.put(VERDANA_BOLD_ITALIC_FONT, "FONTS/verdanaz.ttf");
    }

    public static Map<String, String> getReferencedFonts() {
        return referencedFonts;
    }
}
