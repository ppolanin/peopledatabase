package pl.polanin.support;

import java.util.HashMap;
import java.util.Map;

public class MonthLaboratoryOfPersonIdNumber {

    private final Map<String, MonthOfPersonIdNumber> personIdNumberMonths = new HashMap<>();
    private static final MonthLaboratoryOfPersonIdNumber personIdNumberMonthLaboratory = new MonthLaboratoryOfPersonIdNumber();

    private MonthLaboratoryOfPersonIdNumber() {
    }

    public Map<String, MonthOfPersonIdNumber> getMonthsOfPersonIdNumber() {
        return personIdNumberMonths;
    }

    public static MonthLaboratoryOfPersonIdNumber getInstance() {
        personIdNumberMonthLaboratory.createMonthsOfPersonIdNumber();
        return personIdNumberMonthLaboratory;
    }

    private void createMonthsOfPersonIdNumber() {
        String nineteenthCenturyPrefix = "18";
        String twentiethCenturyPrefix = "19";
        String twentiethFirstCenturyPrefix = "20";
        String twentiethSecondCenturyPrefix = "21";
        String twentiethThirdCenturyPrefix = "22";
        int nineteenthCenturyReferenceNumber = 80;
        int twentiethFirstCenturyReferenceNumber = 20;
        int twentiethSecondCenturyReferenceNumber = 40;
        int twentiethThirdCenturyReferenceNumber = 60;
        for (int i = 1; i <= 12; i++) {
            String nineteenthCenturyMonthOfPersonIdNumber = String.valueOf(nineteenthCenturyReferenceNumber + i);

            String twentiethCenturyMonthOfPersonIdNumber;
            if (i <= 9) {
                twentiethCenturyMonthOfPersonIdNumber = "0" + i;
            } else {
                twentiethCenturyMonthOfPersonIdNumber = String.valueOf(i);
            }
            String twentiethFirstCenturyMonthOfPersonIdNumber = String.valueOf(twentiethFirstCenturyReferenceNumber + i);
            String twentiethSecondCenturyMonthOfPersonIdNumber = String.valueOf(twentiethSecondCenturyReferenceNumber + i);
            String twentiethThirdCenturyMonthOfPersonIdNumber = String.valueOf(twentiethThirdCenturyReferenceNumber + i);

            personIdNumberMonths.put(nineteenthCenturyMonthOfPersonIdNumber, new MonthOfPersonIdNumber(nineteenthCenturyPrefix, nineteenthCenturyMonthOfPersonIdNumber, i));
            personIdNumberMonths.put(twentiethCenturyMonthOfPersonIdNumber, new MonthOfPersonIdNumber(twentiethCenturyPrefix, twentiethCenturyMonthOfPersonIdNumber, i));
            personIdNumberMonths.put(twentiethFirstCenturyMonthOfPersonIdNumber, new MonthOfPersonIdNumber(twentiethFirstCenturyPrefix, twentiethFirstCenturyMonthOfPersonIdNumber, i));
            personIdNumberMonths.put(twentiethSecondCenturyMonthOfPersonIdNumber, new MonthOfPersonIdNumber(twentiethSecondCenturyPrefix, twentiethSecondCenturyMonthOfPersonIdNumber, i));
            personIdNumberMonths.put(twentiethThirdCenturyMonthOfPersonIdNumber, new MonthOfPersonIdNumber(twentiethThirdCenturyPrefix, twentiethThirdCenturyMonthOfPersonIdNumber, i));
        }
    }

    public static class MonthOfPersonIdNumber {

        private final String centuryPrefix;
        private final String centuryMonth;
        private final int monthNumber;

        public MonthOfPersonIdNumber(String centuryPrefix, String centuryMonth, int monthNumber) {
            this.centuryPrefix = centuryPrefix;
            this.centuryMonth = centuryMonth;
            this.monthNumber = monthNumber;
        }

        public String getCenturyPrefix() {
            return centuryPrefix;
        }

        public String getCenturyMonth() {
            return centuryMonth;
        }

        public int getMonthNumber() {
            return monthNumber;
        }

        @Override
        public String toString() {
            return "MonthOfPersonIdNumber{" +
                    "centuryPrefix='" + centuryPrefix + '\'' +
                    ", centuryMonth='" + centuryMonth + '\'' +
                    ", monthNumber=" + monthNumber +
                    '}';
        }
    }
}
