package pl.polanin.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.layout.property.VerticalAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

public class FileExporter<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileExporter.class);

    public void saveDataToTxtFile(List<String> list, File file, String header, String encoding) {
        if (Optional.ofNullable(file).isPresent() && Optional.ofNullable(list).isPresent() && Optional.ofNullable(encoding).isPresent()) {
            try {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding));
                if (Optional.ofNullable(header).isPresent()) {
                    writer.write(header + "\n");
                }
                for (String line : list) {
                    writer.write(line + "\n");
                }
                writer.close();
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    public void saveDataToJsonFile(List<T> list, File file) {
        if (Optional.ofNullable(list).isPresent() && Optional.ofNullable(file).isPresent()) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.writerWithDefaultPrettyPrinter();
                objectMapper.writeValue(file, list);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    public void saveDataToPdfFile(List<String> list, String delimiter, String tableHeader, PdfDocumentConfiguration configuration, File file, String encoding) {

        if (Optional.ofNullable(list).isPresent() && Optional.ofNullable(delimiter).isPresent() && Optional.ofNullable(tableHeader).isPresent() && Optional.ofNullable(configuration).isPresent() && Optional.ofNullable(file).isPresent() && Optional.ofNullable(encoding).isPresent()) {
            try {
                PdfWriter pdfWriter = new PdfWriter(file);
                PdfDocument pdfDocument = new PdfDocument(pdfWriter);
                Document document;
                if (configuration.isPageRotated()) {
                    document = new Document(pdfDocument, configuration.getDocumentPageSize().rotate());
                } else {
                    document = new Document(pdfDocument, configuration.getDocumentPageSize());
                }
                document.setMargins(configuration.getDocumentTopMargin(), configuration.getDocumentRightMargin(), configuration.getDocumentBottomMargin(), configuration.getDocumentLeftMargin());
                PdfFont headerFont = PdfFontFactory.createFont(configuration.getHeaderFont(), encoding, true);
                PdfFont documentFont = PdfFontFactory.createFont(configuration.getDocumentFont(), encoding, true);
                document.add(new Paragraph(configuration.getDocumentHeader())
                        .setFont(headerFont).setFontSize(configuration.getHeaderFontSize())
                        .setTextAlignment(TextAlignment.CENTER));

                Table table = new Table(new float[]{3, 10, 11, 10, 3, 5, 7, 7, 7, 8, 3, 5, 7, 7, 7});
                table.setHorizontalAlignment(HorizontalAlignment.CENTER);
                table.setVerticalAlignment(VerticalAlignment.MIDDLE);

                StringTokenizer headerTokenizer = new StringTokenizer(tableHeader, delimiter);
                while (headerTokenizer.hasMoreTokens()) {
                    table.addHeaderCell(new Cell().add(new Paragraph(headerTokenizer.nextToken())
                            .setFont(documentFont).setFontSize(configuration.getDocumentFontSize()))
                            .setBold()
                            .setVerticalAlignment(VerticalAlignment.MIDDLE)
                            .setTextAlignment(TextAlignment.CENTER));
                }

                for (String line : list) {
                    StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
                    while (tokenizer.hasMoreTokens()) {
                        table.addCell(new Cell().add(new Paragraph(tokenizer.nextToken())
                                .setFont(documentFont).setFontSize(configuration.getDocumentFontSize()))
                                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                                .setTextAlignment(TextAlignment.CENTER));
                    }
                }

                document.add(table);
                document.close();
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else {
            LOGGER.info("Missed parameter/-s to export file to pdf");
        }
    }
}
