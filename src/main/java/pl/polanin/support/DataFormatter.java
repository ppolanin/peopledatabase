package pl.polanin.support;

public class DataFormatter {

    private DataFormatter() {
    }

    public static String trimWhitespace(String word) {
        if (word != null) {
            return word.trim();
        }
        return null;
    }

    public static String clearGaps(String word) {
        if (word != null) {
            String convertedWord = word.trim();
            return convertedWord.replace(" ", "");
        }
        return null;
    }

    public static String convertToLowerCase(String word) {
        if (word != null) {
            return word.toLowerCase();
        }
        return null;
    }

    public static String convertToIndividualName(String words) {
        if (words != null && words.length() > 0) {
            String lowerCaseWords = words.trim().toLowerCase();
            String[] wordContainer = lowerCaseWords.split("\\s");
            String convertedWord;
            String convertedWords;
            if (wordContainer.length > 1) {
                StringBuilder wordsBuilder = new StringBuilder();
                for (String w : wordContainer) {
                    if (w.length() > 1) {
                        convertedWords = w.replaceFirst(w.substring(0, 1), w.substring(0, 1).toUpperCase());
                        w = convertedWords;
                        if (w.startsWith("-")) {
                            String firstPart = w.substring(0, w.indexOf("-") + 1);
                            String lastPart = w.substring(w.indexOf("-") + 1);
                            w = firstPart + lastPart.replaceFirst(lastPart.substring(0, 1), lastPart.substring(0, 1).toUpperCase());
                        }
                    }
                    wordsBuilder.append(w).append(" ");
                }
                return wordsBuilder.toString().trim();
            } else {
                convertedWord = wordContainer[0].toLowerCase();
                return convertedWord.replaceFirst(convertedWord.substring(0, 1), convertedWord.substring(0, 1).toUpperCase());
            }
        }
        return null;
    }
}
