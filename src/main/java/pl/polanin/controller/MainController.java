package pl.polanin.controller;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.polanin.configuration.DbConfigReader;
import pl.polanin.configuration.JDBCConnection;
import pl.polanin.dao.PersonDAOHibernateImpl;
import pl.polanin.domain.Address;
import pl.polanin.domain.Contact;
import pl.polanin.domain.Gender;
import pl.polanin.domain.Person;
import pl.polanin.support.FileExporter;
import pl.polanin.support.PdfDocumentConfiguration;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class MainController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
    private PersonDAOHibernateImpl personDAOH;
    private static final String EMPTY_TABLE_MESSAGE = "";
    private static final String FXML_FILE_NAME_FOR_PERSON_PANE = "PersonPane.fxml";
    private static final String FXML_FILE_NAME_FOR_PDF_SETTINGS_PANE = "PdfExportSettings.fxml";
    private static final String ADD_PERSON_PANE_TITLE = "Dodaj dane";
    private static final String EDIT_PERSON_PANE_TITLE = "Aktualizuj dane";
    private static final String PDF_SETTINGS_PANE_TITLE = "Ustawienia dokumentu pdf";
    private static final String DEFAULT_MESSAGE_FOR_EMPTY_CONTENT = "b.d.";
    private static final String DEFAULT_EMPTY_CONTENT = "";
    private static final String WARNING_TITLE = "Ostrzeżenie";
    private static final String INFORMATION_TITLE = "Informacja";
    private static final String REMOVE_HEADER = "Czy usunąć wskazany rekord?";
    private static final String REMOVE_ALL_HEADER = "Czy usunąć wszystkie rekordy?";
    private static final String NO_SELECTION_HEADER = "Nie wybrano żadnego wiersza";
    private static final String REMOVE_QUESTION_MESSAGE = "Dane zostaną nieodwracalnie usunięte z bazy";
    private static final String NO_SELECTION_MESSAGE = "Zaznacz wiersz";
    private static final String OK_BUTTON_LABEL = "Tak";
    private static final String CANCEL_BUTTON_LABEL = "Anuluj";
    private static final int MIN_PANE_WIDTH = 1010;
    private static final int MIN_PANE_HEIGHT = 620;
    private FileChooser fileChooser;
    private ButtonType okButton;
    private ButtonType cancelButton;
    private PdfSettingsController pdfSettingsController;

    @FXML
    private Parent mainPane;
    @FXML
    private TableView<Person> personTable = new TableView<>();
    @FXML
    private TableColumn<Person, Person> tableColumnOrdinalNumber;
    @FXML
    private TableColumn<Person, String> tableColumnFirstName;
    @FXML
    private TableColumn<Person, String> tableColumnLastName;
    @FXML
    private TableColumn<Person, LocalDate> tableColumnBirthDate;
    @FXML
    private TableColumn<Person, Gender> tableColumnGender;
    @FXML
    private TableColumn<Person, String> tableColumnNationality;
    @FXML
    private TableColumn<Person, String> tableColumnPersonalId;
    @FXML
    private Label countryPersonInfo;
    @FXML
    private Label cityPersonInfo;
    @FXML
    private Label streetPersonInfo;
    @FXML
    private Label buildingNoPersonInfo;
    @FXML
    private Label zipCodePersonInfo;
    @FXML
    private Label emailPersonInfo;
    @FXML
    private Label mobilePhoneNoPersonInfo;
    @FXML
    private Label linedBasedPhoneNoPersonInfo;
    @FXML
    private Button openAddPersonDataPane;
    @FXML
    private Button openEditPersonDataPane;
    @FXML
    private Button deletePersonDataButton;
    @FXML
    private MenuItem openAddPersonDataPaneFromMenuItem;
    @FXML
    private MenuItem openEditPersonDataPaneFromMenuItem;
    @FXML
    private MenuItem deletePersonDataFromMenuItem;
    @FXML
    private MenuItem deletePeopleDataFromMenuItem;
    @FXML
    private MenuItem saveDataToFileFromMenuItem;
    @FXML
    private MenuItem closeAppFromMenuItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            okButton = new ButtonType(OK_BUTTON_LABEL, ButtonBar.ButtonData.OK_DONE);
            cancelButton = new ButtonType(CANCEL_BUTTON_LABEL, ButtonBar.ButtonData.CANCEL_CLOSE);
            personDAOH = new PersonDAOHibernateImpl();
            JDBCConnection jdbcConnection = new JDBCConnection(DbConfigReader.loadDbConfiguration());
            createDB(jdbcConnection);
            addButtonEventHandlers();
            addMenuItemHandlers();
            initialFileChooser();
            loadTable();
        } catch (IOException ioEx) {
            LOGGER.error(ioEx.getMessage(), ioEx);
        }
    }

    private void addButtonEventHandlers() {
        openAddPersonDataPane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            openPaneToAddPersonData();
            event.consume();
        });

        openEditPersonDataPane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            openPaneToEditPersonData();
            event.consume();
        });

        deletePersonDataButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            removePerson();
            event.consume();
        });
    }

    private void addMenuItemHandlers() {
        openAddPersonDataPaneFromMenuItem.setOnAction(event -> {
            openPaneToAddPersonData();
            event.consume();
        });

        openEditPersonDataPaneFromMenuItem.setOnAction(event -> {
            openPaneToEditPersonData();
            event.consume();
        });

        deletePersonDataFromMenuItem.setOnAction(event -> {
            removePerson();
            event.consume();
        });

        deletePeopleDataFromMenuItem.setOnAction(event -> {
            removePeople();
            event.consume();
        });

        closeAppFromMenuItem.setOnAction(event -> {
            Stage mainStage = (Stage) mainPane.getScene().getWindow();
            mainStage.close();
            event.consume();
        });

        saveDataToFileFromMenuItem.setOnAction(event -> {
            File file = fileChooser.showSaveDialog(mainPane.getScene().getWindow());
            if (Optional.ofNullable(file).isPresent()) {
                fileChooser.setInitialDirectory(file.getParentFile());
                saveDataToFile(file, personDAOH.getPeople());
            } else {
                LOGGER.info("File is null");
            }
            event.consume();
        });
    }

    private void loadTable() {
        tableColumnOrdinalNumber.setCellValueFactory(p -> new ReadOnlyObjectWrapper<>(p.getValue()));
        tableColumnOrdinalNumber.setCellFactory(new Callback<TableColumn<Person, Person>, TableCell<Person, Person>>() {
            @Override
            public TableCell<Person, Person> call(TableColumn<Person, Person> param) {
                return new TableCell<Person, Person>() {
                    @Override
                    protected void updateItem(Person item, boolean empty) {
                        super.updateItem(item, empty);

                        if (this.getTableRow() != null && item != null) {
                            setText(this.getTableRow().getIndex() + 1 + "");
                        } else {
                            setText("");
                        }
                    }
                };
            }
        });
        tableColumnOrdinalNumber.setSortable(false);

        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tableColumnBirthDate.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));
        tableColumnGender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        tableColumnNationality.setCellValueFactory(new PropertyValueFactory<>("nationality"));
        tableColumnPersonalId.setCellValueFactory(new PropertyValueFactory<>("personalNumber"));
        ObservableList<Person> peopleList = FXCollections.observableArrayList();
        peopleList.addAll(personDAOH.getPeople());
        personTable.setItems(peopleList);
        personTable.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> displayAdditionalPersonInfo(newValue)));

        personTable.setRowFactory(param -> {
            final TableRow<Person> row = new TableRow<>();
            row.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
                final int rowIndex = row.getIndex();
                if (rowIndex >= 0 && rowIndex < personTable.getItems().size() && personTable.getSelectionModel().isSelected(rowIndex)) {
                    personTable.getSelectionModel().clearSelection(rowIndex);
                    event.consume();
                }
            });
            return row;
        });

        personTable.setPlaceholder(new Label(EMPTY_TABLE_MESSAGE));
        personTable.setMinSize(842, 780);
    }

    private void updateTable() {
        ObservableList<Person> peopleList = FXCollections.observableArrayList();
        peopleList.addAll(personDAOH.getPeople());
        personTable.getItems().removeAll();
        clearAdditionalPersonInfo();
        personTable.setItems(peopleList);
        personTable.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> displayAdditionalPersonInfo(newValue)));
    }

    private void removePeople() {
        Alert warningAlert = new Alert(Alert.AlertType.WARNING, REMOVE_QUESTION_MESSAGE, okButton, cancelButton);
        warningAlert.setTitle(WARNING_TITLE);
        warningAlert.setHeaderText(REMOVE_ALL_HEADER);
        warningAlert.showAndWait().ifPresent(response -> {
            if (response.getButtonData().equals(okButton.getButtonData()) && personTable.getItems().size() > 0) {
                personDAOH.removePeople();
                updateTable();
            } else if (response.getButtonData().equals(cancelButton.getButtonData())) {
                warningAlert.close();
            }
        });
    }

    private void displayAdditionalPersonInfo(Person person) {
        if (person != null) {
            Address personAddress = person.getAddress();
            if (personAddress != null) {
                countryPersonInfo.setText(Optional.ofNullable(personAddress.getCountry()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
                cityPersonInfo.setText(Optional.ofNullable(personAddress.getCity()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
                streetPersonInfo.setText(Optional.ofNullable(personAddress.getStreet()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
                buildingNoPersonInfo.setText(Optional.ofNullable(personAddress.getBuildingNo()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
                zipCodePersonInfo.setText(Optional.ofNullable(personAddress.getZipCode()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
            } else {
                countryPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
                cityPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
                streetPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
                buildingNoPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
                zipCodePersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
            }
            Contact personContact = person.getContact();
            if (personContact != null) {
                emailPersonInfo.setText(Optional.ofNullable(personContact.getEmail()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
                mobilePhoneNoPersonInfo.setText(Optional.ofNullable(personContact.getMobileNumber()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
                linedBasedPhoneNoPersonInfo.setText(Optional.ofNullable(personContact.getLandLine()).orElse(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT));
            } else {
                emailPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
                mobilePhoneNoPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
                linedBasedPhoneNoPersonInfo.setText(DEFAULT_MESSAGE_FOR_EMPTY_CONTENT);
            }
        }
    }

    private void clearAdditionalPersonInfo() {
        countryPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        cityPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        streetPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        buildingNoPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        zipCodePersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        emailPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        mobilePhoneNoPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
        linedBasedPhoneNoPersonInfo.setText(DEFAULT_EMPTY_CONTENT);
    }

    private void openPaneToAddPersonData() {
        URL personPaneResource = getClass().getClassLoader().getResource(MainController.FXML_FILE_NAME_FOR_PERSON_PANE);
        if (personPaneResource != null) {
            try {
                FXMLLoader loader = new FXMLLoader(personPaneResource);
                Pane personGUI = loader.load();
                Stage personStage = new Stage();
                personStage.initModality(Modality.APPLICATION_MODAL);
                personStage.setTitle(MainController.ADD_PERSON_PANE_TITLE);
                personStage.setScene(new Scene(personGUI, MainController.MIN_PANE_WIDTH, MainController.MIN_PANE_HEIGHT));
                personStage.setResizable(false);
                PersonController personController = loader.getController();
                personController.disableUpdateButton(true);
                personStage.showAndWait();
                updateTable();
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else {
            LOGGER.info("Resource is null");
        }
    }

    private void openPaneToEditPersonData() {
        int selectedTableRowIndex = personTable.getSelectionModel().getSelectedIndex();
        if (selectedTableRowIndex >= 0) {
            Person person = personTable.getSelectionModel().getSelectedItem();
            URL personPaneResource = getClass().getClassLoader().getResource(MainController.FXML_FILE_NAME_FOR_PERSON_PANE);
            if (personPaneResource != null) {
                try {
                    FXMLLoader loader = new FXMLLoader(personPaneResource);
                    Pane personGUI = loader.load();
                    Stage personStage = new Stage();
                    personStage.initModality(Modality.APPLICATION_MODAL);
                    personStage.setTitle(MainController.EDIT_PERSON_PANE_TITLE);
                    personStage.setScene(new Scene(personGUI, MainController.MIN_PANE_WIDTH, MainController.MIN_PANE_HEIGHT));
                    personStage.setResizable(false);
                    PersonController personController = loader.getController();
                    personController.setPersonToUpdate(person);
                    personController.fillDataFromPerson();
                    personController.disableSaveButton(true);
                    personStage.showAndWait();
                    updateTable();
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            } else {
                LOGGER.info("Resource is null");
            }
        } else {
            showNoSelectionAlert();
        }
    }

    private void openPdfDocumentSettings() {
        URL pdfConfigurationResource = getClass().getClassLoader().getResource(FXML_FILE_NAME_FOR_PDF_SETTINGS_PANE);
        if (pdfConfigurationResource != null) {
            try {
                FXMLLoader loader = new FXMLLoader(pdfConfigurationResource);
                Pane pdfConfigurationPane = loader.load();
                pdfSettingsController = loader.getController();
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(new Scene(pdfConfigurationPane));
                stage.setTitle(PDF_SETTINGS_PANE_TITLE);
                stage.showAndWait();
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else {
            LOGGER.info("Resource is null");
        }
    }

    private void removePerson() {
        int selectedTableRowIndex = personTable.getSelectionModel().getSelectedIndex();
        if (selectedTableRowIndex >= 0) {
            Alert confirmationAlert = new Alert(Alert.AlertType.WARNING, REMOVE_QUESTION_MESSAGE, okButton, cancelButton);
            confirmationAlert.setTitle(WARNING_TITLE);
            confirmationAlert.setHeaderText(REMOVE_HEADER);
            confirmationAlert.showAndWait().ifPresent(response -> {
                if (response.getButtonData().equals(okButton.getButtonData())) {
                    Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
                    personDAOH.removePersonData(selectedPerson);
                    updateTable();
                } else if (response.getButtonData().equals(cancelButton.getButtonData())) {
                    confirmationAlert.close();
                }
            });
        } else {
            showNoSelectionAlert();
        }
    }

    private void showNoSelectionAlert() {
        Alert noSelectionAlert = new Alert(Alert.AlertType.INFORMATION, NO_SELECTION_MESSAGE, ButtonType.OK);
        noSelectionAlert.setTitle(INFORMATION_TITLE);
        noSelectionAlert.setHeaderText(NO_SELECTION_HEADER);
        noSelectionAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                noSelectionAlert.close();
            }
        });
    }

    private void createDB(JDBCConnection jdbcConnection) {
        try (Connection connection = jdbcConnection.connectionToDB();
             Statement statement = connection.createStatement()) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("src/main/resources/personDB.sql"));
            String sqlStatement = bufferedReader.lines().collect(Collectors.joining("\n"));
            LOGGER.info(sqlStatement);
            statement.executeUpdate(sqlStatement);
        } catch (FileNotFoundException | SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void initialFileChooser() {
        fileChooser = new FileChooser();
        fileChooser.setTitle("Zapisz jako...");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("wszystkie pliki", "*.*"),
                new FileChooser.ExtensionFilter("zwykły tekst", "*.txt"),
                new FileChooser.ExtensionFilter("CSV (rozdzielany średnikami)", "*.csv"),
                new FileChooser.ExtensionFilter("JSON", "*.json"),
                new FileChooser.ExtensionFilter("PDF", "*.pdf")
        );
    }

    private List<String> createTextPersonData(List<Person> people, String delimiter) {
        ArrayList<String> textPersonList = new ArrayList<>();
        String noContentValue = "b.d.";
        if (Optional.ofNullable(people).isPresent() && Optional.ofNullable(delimiter).isPresent()) {
            for (Person person : people) {
                StringBuilder textPersonBuilder = new StringBuilder();
                textPersonBuilder.append(person.getPersonId()).append(delimiter).append(person.getFirstName()).append(delimiter).append(person.getLastName()).append(delimiter).append(person.getDateOfBirth()).append(delimiter).append(person.getGender());
                if (Optional.ofNullable(person.getNationality()).isPresent()) {
                    textPersonBuilder.append(delimiter).append(person.getNationality());
                } else {
                    textPersonBuilder.append(delimiter).append(noContentValue);
                }
                textPersonBuilder.append(delimiter).append(person.getPersonalNumber())
                        .append(delimiter).append(person.getAddress().getCountry()).append(delimiter).append(person.getAddress().getCity()).append(delimiter).append(person.getAddress().getStreet()).append(delimiter).append(person.getAddress().getBuildingNo()).append(delimiter).append(person.getAddress().getZipCode());
                if (Optional.ofNullable(person.getContact().getEmail()).isPresent()) {
                    textPersonBuilder.append(delimiter).append(person.getContact().getEmail());
                } else {
                    textPersonBuilder.append(delimiter).append(noContentValue);
                }
                textPersonBuilder.append(delimiter).append(person.getContact().getMobileNumber());
                if (Optional.ofNullable(person.getContact().getLandLine()).isPresent()) {
                    textPersonBuilder.append(delimiter).append(person.getContact().getLandLine());
                } else {
                    textPersonBuilder.append(delimiter).append(noContentValue);
                }
                textPersonList.add(textPersonBuilder.toString());
            }
            return textPersonList;
        }
        return null;
    }

    private void saveDataToFile(File file, List<Person> personList) {
        String defaultTxtCoding = "UTF8";
        String defaultCsvCoding = "Cp1250";
        String txtExtensionFile = "txt";
        String csvExtensionFile = "csv";
        String pdfExtensionFile = "pdf";
        String defaultTxtDelimiter = ",";
        String defaultCsvDelimiter = ";";
        FileExporter<String> fileExporter = new FileExporter<>();
        if (Optional.ofNullable(file).isPresent() && Optional.ofNullable(personList).isPresent()) {
            String fileExtension = file.getName().substring(file.getName().length() - 3);
            StringBuilder csvHeader = new StringBuilder("id");
            csvHeader.append(defaultCsvDelimiter).append("imię").append(defaultCsvDelimiter).append("nazwisko").append(defaultCsvDelimiter).append("data urodzenia").append(defaultCsvDelimiter).append("płeć")
                    .append(defaultCsvDelimiter).append("obywatelstwo").append(defaultCsvDelimiter).append("PESEL").append(defaultCsvDelimiter).append("kraj")
                    .append(defaultCsvDelimiter).append("miasto").append(defaultCsvDelimiter).append("ulica").append(defaultCsvDelimiter).append("nr budynku").append(defaultCsvDelimiter).append("kod pocztowy")
                    .append(defaultCsvDelimiter).append("email").append(defaultCsvDelimiter).append("telefon komórkowy").append(defaultCsvDelimiter).append("telefon stacjonarny");
            if (fileExtension.equals(txtExtensionFile)) {
                String txtHeader = "id" + defaultTxtDelimiter + "imię" + defaultTxtDelimiter + "nazwisko" + defaultTxtDelimiter + "data urodzenia" + defaultTxtDelimiter + "płeć" +
                        defaultTxtDelimiter + "obywatelstwo" + defaultTxtDelimiter + "PESEL" + defaultTxtDelimiter + "kraj" +
                        defaultTxtDelimiter + "miasto" + defaultTxtDelimiter + "ulica" + defaultTxtDelimiter + "nr budynku" + defaultTxtDelimiter + "kod pocztowy" +
                        defaultTxtDelimiter + "email" + defaultTxtDelimiter + "telefon komórkowy" + defaultTxtDelimiter + "telefon stacjonarny";
                fileExporter.saveDataToTxtFile(createTextPersonData(personList, defaultTxtDelimiter), file, txtHeader, defaultTxtCoding);
            } else if (fileExtension.equals(csvExtensionFile)) {
                fileExporter.saveDataToTxtFile(createTextPersonData(personList, defaultCsvDelimiter), file, csvHeader.toString(), defaultCsvCoding);
            } else if (fileExtension.equals(pdfExtensionFile)) {
                openPdfDocumentSettings();
                PdfDocumentConfiguration configuration = pdfSettingsController.getConfiguration();
                fileExporter.saveDataToPdfFile(createTextPersonData(personList, defaultCsvDelimiter), defaultCsvDelimiter, csvHeader.toString(), configuration, file, defaultCsvCoding);
            } else {
                FileExporter<Person> jsonFileExporter = new FileExporter<>();
                jsonFileExporter.saveDataToJsonFile(personList, file);
            }
        }
    }

}
