package pl.polanin.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.polanin.dao.PersonDAOHibernateImpl;
import pl.polanin.datavalidation.DataValidation;
import pl.polanin.domain.Address;
import pl.polanin.domain.Contact;
import pl.polanin.domain.Gender;
import pl.polanin.domain.Person;
import pl.polanin.support.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class PersonController implements Initializable {

    private static final String REQUIRED_FILLED_MESSAGE = "Pole wymagane";
    private static final String EMPTY_CONTENT = "";
    private static final String INFORMATION_TITLE = "Informacja";
    private static final String DUPLICATION_MESSAGE = "Osoba już istnieje w bazie";
    private static final String DUPLICATION_HEADER = "Powtórzenie danych";
    private static final String INVALID_NAME_MESSAGE = "nieprawidłowa długość wyrazu lub nieprawidłowe znaki";
    private static final String INVALID_BIRTH_DATE_MESSAGE = "nieprawidłowa data [format: dd.mm.rrrr]";
    private static final String REQUIRED_GENDER_MESSAGE = "wybierz płeć";
    private static final String INVALID_IDENTIFICATION_NUMBER_MESSAGE = "nieprawidłowy PESEL [Sprawdź datę urodzenia i płeć]";
    private static final String INVALID_ZIP_CODE_MESSAGE = "nieprawidłowy kod [format: 00-000]";
    private static final String INVALID_MOBILE_PHONE_NUMBER_MESSAGE = "nieprawidłowy numer [format: +00 000 000 000]";
    private static final String INVALID_LAND_BASE_PHONE_NUMBER_MESSAGE = "nieprawidłowy numer [format: 00 000 00 00]";
    private static final String INVALID_MAIL_MESSAGE = "nieprawidłowy format adresu mailowego";
    private static final String PERSON_NAME_ID = "name";
    private static final String PERSON_SURNAME_ID = "surname";
    private static final String PERSON_BIRTH_DATE_ID = "birth date";
    private static final String PERSON_NATIONALITY_ID = "nationality";
    private static final String PERSON_IDENTIFICATION_NUMBER_ID = "id number";
    private static final String PERSON_COUNTRY_ID = "country";
    private static final String PERSON_CITY_ID = "city";
    private static final String PERSON_STREET_ID = "street";
    private static final String PERSON_BUILDING_NO_ID = "building number";
    private static final String PERSON_ZIP_CODE_ID = "zip code";
    private static final String PERSON_MAIL_ID = "mail";
    private static final String PERSON_MOBILE_NUMBER_ID = "mobile phone number";
    private static final String PERSON_LAND_LINE_NUMBER_ID = "land line phone number";
    private List<TextCompositeCollector> textElementsList;
    private PersonDAOHibernateImpl personDAOH;
    private Person personToUpdate;

    @FXML
    private Parent Pane;
    @FXML
    private TextField personNameTextField;
    @FXML
    private TextField personSurnameTextField;
    @FXML
    private TextField personBirthDateTextField;
    @FXML
    private ChoiceBox<String> personGenderChoiceBox;
    @FXML
    private TextField personNationalityTextField;
    @FXML
    private TextField personIDNumberTextField;
    @FXML
    private TextField personCountryTextField;
    @FXML
    private TextField personCityTextField;
    @FXML
    private TextField personStreetTextField;
    @FXML
    private TextField personBuildingNoTextField;
    @FXML
    private TextField personZipCodeTextField;
    @FXML
    private TextField personMailTextField;
    @FXML
    private TextField personMobilePhoneTextField;
    @FXML
    private TextField addPersonLandLinePhoneTextField;
    @FXML
    private Button saveButton;
    @FXML
    Button updateButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Label personNameInfoLabel;
    @FXML
    private Label personSurnameInfoLabel;
    @FXML
    private Label personBirthDateInfoLabel;
    @FXML
    private Label personGenderInfoLabel;
    @FXML
    private Label personNationalityInfoLabel;
    @FXML
    private Label personIDNumberInfoLabel;
    @FXML
    private Label personCountryInfoLabel;
    @FXML
    private Label personCityInfoLabel;
    @FXML
    private Label personStreetInfoLabel;
    @FXML
    private Label personBuildingNoInfoLabel;
    @FXML
    private Label personZipCodeInfoLabel;
    @FXML
    private Label personMailInfoLabel;
    @FXML
    private Label personMobilePhoneInfoLabel;
    @FXML
    private Label personLandLinePhoneInfoLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.personGenderChoiceBox.setItems(FXCollections.observableArrayList("M", "K"));
        createTextElementsList();
        addButtonEventListeners();
        personDAOH = new PersonDAOHibernateImpl();
    }

    public void disableUpdateButton(boolean flag) {
        updateButton.setDisable(flag);
    }

    public void disableSaveButton(boolean flag) {
        saveButton.setDisable(flag);
    }

    public void setPersonToUpdate(Person personToUpdate) {
        this.personToUpdate = personToUpdate;
    }

    private void addButtonEventListeners() {
        saveButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            Person person = createPerson();
            if (isFormProperlyFilled() && person != null) {
                if (!personDAOH.isPersonExist(person)) {
                    personDAOH.addPersonData(person);
                    Stage stage = (Stage) Pane.getScene().getWindow();
                    stage.close();
                } else {
                    Alert personExistAlert = new Alert(Alert.AlertType.INFORMATION, DUPLICATION_MESSAGE, ButtonType.OK);
                    personExistAlert.setTitle(INFORMATION_TITLE);
                    personExistAlert.setHeaderText(DUPLICATION_HEADER);
                    personExistAlert.showAndWait().ifPresent(response -> {
                        if (response.getButtonData().isDefaultButton()) {
                            personExistAlert.close();
                        }
                    });
                }
            }
            event.consume();
        });

        updateButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            Person updatedPersonData = createPerson();
            if (isFormProperlyFilled() && updatedPersonData != null) {
                if (!personToUpdate.equals(updatedPersonData)) {
                    personToUpdate.setFirstName(updatedPersonData.getFirstName());
                    personToUpdate.setLastName(updatedPersonData.getLastName());
                    personToUpdate.setGender(updatedPersonData.getGender());
                    personToUpdate.setPersonalNumber(updatedPersonData.getPersonalNumber());
                    personToUpdate.setDateOfBirth(updatedPersonData.getDateOfBirth());
                    personToUpdate.setNationality(updatedPersonData.getNationality());
                    personToUpdate.setAddress(updatedPersonData.getAddress());
                    personToUpdate.setContact(updatedPersonData.getContact());
                    personDAOH.updatePersonData(personToUpdate);
                }
                Stage stage = (Stage) Pane.getScene().getWindow();
                stage.close();
            }
            event.consume();
        });

        cancelButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
                event -> {
                    Stage addPersonStage = (Stage) Pane.getScene().getWindow();
                    addPersonStage.close();
                    event.consume();
                });
    }

    private void createTextElementsList() {
        textElementsList = new ArrayList<>();
        textElementsList.add(new TextCompositeCollector(PERSON_NAME_ID, personNameTextField, personNameInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_SURNAME_ID, personSurnameTextField, personSurnameInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_BIRTH_DATE_ID, personBirthDateTextField, personBirthDateInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_NATIONALITY_ID, personNationalityTextField, personNationalityInfoLabel, false));
        textElementsList.add(new TextCompositeCollector(PERSON_IDENTIFICATION_NUMBER_ID, personIDNumberTextField, personIDNumberInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_COUNTRY_ID, personCountryTextField, personCountryInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_CITY_ID, personCityTextField, personCityInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_STREET_ID, personStreetTextField, personStreetInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_BUILDING_NO_ID, personBuildingNoTextField, personBuildingNoInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_ZIP_CODE_ID, personZipCodeTextField, personZipCodeInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_MAIL_ID, personMailTextField, personMailInfoLabel, false));
        textElementsList.add(new TextCompositeCollector(PERSON_MOBILE_NUMBER_ID, personMobilePhoneTextField, personMobilePhoneInfoLabel, true));
        textElementsList.add(new TextCompositeCollector(PERSON_LAND_LINE_NUMBER_ID, addPersonLandLinePhoneTextField, personLandLinePhoneInfoLabel, false));
    }

    private void displayMessageForFieldFormGap(TextCompositeCollector textComposite, String message) {
        textComposite.getLabel().setText(message);
    }

    private boolean isPersonFieldFormFilled(TextCompositeCollector textComposite) {
        return !textComposite.getTextField().getText().isEmpty();
    }

    private Person createPerson() {
        String personName = null;
        String personSurname = null;
        LocalDate personDateBirth = null;
        Gender personGender = null;
        String personNationality = null;
        String personIDNumber = null;
        String personCountry = null;
        String personCity = null;
        String personStreet = null;
        String personBuildingNo = null;
        String personZipCode = null;
        String personMail = null;
        String personMobilePhoneNumber = null;
        String personLandLinePhoneNumber = null;

        if (personGenderChoiceBox.getSelectionModel().isEmpty()) {
            personGenderInfoLabel.setText(REQUIRED_GENDER_MESSAGE);
        } else {
            personGenderInfoLabel.setText(EMPTY_CONTENT);
            personGender = Gender.valueOf(personGenderChoiceBox.getValue());
        }

        for (TextCompositeCollector textElement : textElementsList) {
            if (textElement.isRequired()) {
                if (!isPersonFieldFormFilled(textElement)) {
                    displayMessageForFieldFormGap(textElement, REQUIRED_FILLED_MESSAGE);
                } else {
                    displayMessageForFieldFormGap(textElement, EMPTY_CONTENT);

                    if (PERSON_NAME_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.trimWhitespace(textElement.getTextField().getText()));
                        textElement.getTextField().setText(DataFormatter.convertToIndividualName(textElement.getTextField().getText()));
                        if (!DataValidation.isNameValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_NAME_MESSAGE);
                        } else {
                            personName = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_SURNAME_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.trimWhitespace(textElement.getTextField().getText()));
                        textElement.getTextField().setText(DataFormatter.convertToIndividualName(textElement.getTextField().getText()));
                        if (!DataValidation.isNameWithDashValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_NAME_MESSAGE);
                        } else {
                            personSurname = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_BIRTH_DATE_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.clearGaps(textElement.getTextField().getText()));
                        if (!DataValidation.isDateValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_BIRTH_DATE_MESSAGE);
                        } else {
                            personDateBirth = DataConverter.convertDateToSQLDateFormat(textElement.getTextField().getText());
                        }
                    }

                    if (PERSON_IDENTIFICATION_NUMBER_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.clearGaps(textElement.getTextField().getText()));
                        if (!DataValidation.isPersonIdNumberValid(textElement.getTextField().getText(), personDateBirth, personGender)) {
                            textElement.getLabel().setText(INVALID_IDENTIFICATION_NUMBER_MESSAGE);
                        } else {
                            personIDNumber = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_COUNTRY_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.trimWhitespace(textElement.getTextField().getText()));
                        textElement.getTextField().setText(DataFormatter.convertToIndividualName(textElement.getTextField().getText()));
                        if (!DataValidation.isNameValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_NAME_MESSAGE);
                        } else {
                            personCountry = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_CITY_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.trimWhitespace(textElement.getTextField().getText()));
                        textElement.getTextField().setText(DataFormatter.convertToIndividualName(textElement.getTextField().getText()));
                        if (!DataValidation.isAddressElementValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_NAME_MESSAGE);
                        } else {
                            personCity = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_STREET_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.trimWhitespace(textElement.getTextField().getText()));
                        textElement.getTextField().setText(DataFormatter.convertToIndividualName(textElement.getTextField().getText()));
                        if (!DataValidation.isAddressElementValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_NAME_MESSAGE);
                        } else {
                            personStreet = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_BUILDING_NO_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.clearGaps(textElement.getTextField().getText()));
                        if (!DataValidation.isBuildingNoValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_NAME_MESSAGE);
                        } else {
                            personBuildingNo = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_ZIP_CODE_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.clearGaps(textElement.getTextField().getText()));
                        if (!DataValidation.isZipCodeValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_ZIP_CODE_MESSAGE);
                        } else {
                            personZipCode = textElement.getTextField().getText();
                        }
                    }

                    if (PERSON_MOBILE_NUMBER_ID.equals(textElement.getId())) {
                        textElement.getTextField().setText(DataFormatter.clearGaps(textElement.getTextField().getText()));
                        if (!DataValidation.isMobilePhoneNumberValid(textElement.getTextField().getText())) {
                            textElement.getLabel().setText(INVALID_MOBILE_PHONE_NUMBER_MESSAGE);
                        } else {
                            personMobilePhoneNumber = textElement.getTextField().getText();
                        }
                    }
                }
            }
        }

        if (personMailTextField.getText() != null && !EMPTY_CONTENT.equals(personMailTextField.getText())) {
            personMailTextField.setText(DataFormatter.clearGaps(personMailTextField.getText()));
            personMailTextField.setText(DataFormatter.convertToLowerCase(personMailTextField.getText()));
            if (!DataValidation.isMailValid(personMailTextField.getText())) {
                personMailInfoLabel.setText(INVALID_MAIL_MESSAGE);
            } else {
                personMailInfoLabel.setText(EMPTY_CONTENT);
                personMail = personMailTextField.getText();
            }
        }

        if (addPersonLandLinePhoneTextField.getText() != null && !EMPTY_CONTENT.equals(addPersonLandLinePhoneTextField.getText())) {
            addPersonLandLinePhoneTextField.setText(DataFormatter.clearGaps(addPersonLandLinePhoneTextField.getText()));
            if (!DataValidation.isLandBasedPhoneNumberValid(addPersonLandLinePhoneTextField.getText())) {
                personLandLinePhoneInfoLabel.setText(INVALID_LAND_BASE_PHONE_NUMBER_MESSAGE);
            } else {
                personLandLinePhoneInfoLabel.setText(EMPTY_CONTENT);
                personLandLinePhoneNumber = addPersonLandLinePhoneTextField.getText();
            }
        }

        if (personNationalityTextField.getText() != null && !EMPTY_CONTENT.equals(personNationalityTextField.getText())) {
            personNationalityTextField.setText(DataFormatter.trimWhitespace(personNationalityTextField.getText()));
            personNationalityTextField.setText(DataFormatter.convertToLowerCase(personNationalityTextField.getText()));
            if (!DataValidation.isNameValid(personNationalityTextField.getText())) {
                personNationalityInfoLabel.setText(INVALID_NAME_MESSAGE);
            } else {
                personNationalityInfoLabel.setText(EMPTY_CONTENT);
                personNationality = personNationalityTextField.getText();
            }
        }
        if (personName != null && personSurname != null && personDateBirth != null && personGender != null && personIDNumber != null && personCountry != null
                && personCity != null && personStreet != null && personBuildingNo != null && personZipCode != null && personMobilePhoneNumber != null) {

            Address personAddress = new Address(personCountry, personCity, personStreet, personBuildingNo, personZipCode);
            Contact personContact = new Contact(personMobilePhoneNumber);
            Person person = new Person(personName, personSurname, personDateBirth, personIDNumber, personAddress, personContact, personGender);

            if (personNationality != null) {
                person.setNationality(personNationality);
            }
            if (personMail != null) {
                personContact.setEmail(personMail);
                person.setContact(personContact);
            }
            if (personLandLinePhoneNumber != null) {
                personContact.setLandLine(personLandLinePhoneNumber);
                person.setContact(personContact);
            }
            return person;
        } else {
            return null;
        }
    }

    private boolean isFormProperlyFilled() {
        long alertCounter = textElementsList.stream().filter(element -> !element.getLabel().getText().equals(EMPTY_CONTENT)).count();
        return alertCounter <= 0;
    }

    public void fillDataFromPerson() {
        personNameTextField.setText(personToUpdate.getFirstName());
        personSurnameTextField.setText(personToUpdate.getLastName());
        personBirthDateTextField.setText(DataConverter.convertSQLDateToDotDateFormat(personToUpdate.getDateOfBirth().toString()));
        personGenderChoiceBox.setValue(personToUpdate.getGender().getStringGender());
        personNationalityTextField.setText(personToUpdate.getNationality());
        personIDNumberTextField.setText(personToUpdate.getPersonalNumber());
        personCountryTextField.setText(personToUpdate.getAddress().getCountry());
        personCityTextField.setText(personToUpdate.getAddress().getCity());
        personStreetTextField.setText(personToUpdate.getAddress().getStreet());
        personBuildingNoTextField.setText(personToUpdate.getAddress().getBuildingNo());
        personZipCodeTextField.setText(personToUpdate.getAddress().getZipCode());
        personMailTextField.setText(personToUpdate.getContact().getEmail());
        personMobilePhoneTextField.setText(personToUpdate.getContact().getMobileNumber());
        addPersonLandLinePhoneTextField.setText(personToUpdate.getContact().getLandLine());
    }
}
