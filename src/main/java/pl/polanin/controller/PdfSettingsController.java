package pl.polanin.controller;

import com.itextpdf.kernel.geom.PageSize;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.polanin.support.PdfDocumentConfiguration;
import pl.polanin.support.ReferencedFont;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class PdfSettingsController implements Initializable {

    private PdfDocumentConfiguration configuration;
    private Map<String, PageSize> pageFormats;
    private static final String PAGE_FORMAT_A4 = "A4";
    private static final String PAGE_FORMAT_A3 = "A3";
    private static final String VERTICAL_PAGE_ORIENTATION = "pionowa";
    private static final String HORIZONTAL_PAGE_ORIENTATION = "pozioma";

    @FXML
    private Parent pdfDocumentConfigurationPane;
    @FXML
    private TextField documentHeader;
    @FXML
    private ChoiceBox<String> headerFont;
    @FXML
    private ChoiceBox<Float> headerFontSize;
    @FXML
    private ChoiceBox<Float> documentFontSize;
    @FXML
    private ChoiceBox<String> documentFont;
    @FXML
    private ChoiceBox<String> documentPageSize;
    @FXML
    private ChoiceBox<String> documentOrientation;
    @FXML
    private Spinner<Integer> topDocumentMargin;
    @FXML
    private Spinner<Integer> leftDocumentMargin;
    @FXML
    private Spinner<Integer> rightDocumentMargin;
    @FXML
    private Spinner<Integer> bottomDocumentMargin;
    @FXML
    private Button acceptPdfSettingsButton;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> fontNames = FXCollections.observableArrayList(ReferencedFont.getReferencedFonts().keySet());
        ObservableList<Float> fontSizes = FXCollections.observableArrayList(6f, 8f, 10f, 12f, 14f, 18f, 20f, 22f, 24f, 26f, 28f, 36f, 48f, 72f);

        pageFormats = new HashMap<>();
        pageFormats.put(PAGE_FORMAT_A4, PageSize.A4);
        pageFormats.put(PAGE_FORMAT_A3, PageSize.A3);

        headerFont.setItems(fontNames);
        headerFont.setValue(ReferencedFont.TIMES_NEW_ROMAN_REGULAR_FONT);
        headerFontSize.setItems(fontSizes);
        headerFontSize.setValue(fontSizes.get(3));
        documentFont.setItems(fontNames);
        documentFont.setValue(ReferencedFont.TIMES_NEW_ROMAN_REGULAR_FONT);
        documentFontSize.setItems(fontSizes);
        documentFontSize.setValue(fontSizes.get(2));
        documentOrientation.setItems(FXCollections.observableArrayList(VERTICAL_PAGE_ORIENTATION, HORIZONTAL_PAGE_ORIENTATION));
        documentOrientation.setValue(VERTICAL_PAGE_ORIENTATION);
        documentPageSize.setItems(FXCollections.observableArrayList(pageFormats.keySet()));
        documentPageSize.setValue(PAGE_FORMAT_A4);
        topDocumentMargin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 10));
        rightDocumentMargin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 10));
        leftDocumentMargin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 10));
        bottomDocumentMargin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 10));
        acceptPdfSettingsButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setPdfDocumentConfiguration();
            Stage stage = (Stage) pdfDocumentConfigurationPane.getScene().getWindow();
            stage.close();
            event.consume();
        });
    }

    private void setPdfDocumentConfiguration() {
        configuration = new PdfDocumentConfiguration.PdfDocumentConfigurationBuilder()
                .setHeaderFont(ReferencedFont.getReferencedFonts().getOrDefault(headerFont.getValue(), ReferencedFont.TIMES_NEW_ROMAN_REGULAR_FONT))
                .setHeaderFontSize(headerFontSize.getValue())
                .setDocumentHeader(documentHeader.getText())
                .setDocumentFont(ReferencedFont.getReferencedFonts().getOrDefault(documentFont.getValue(), ReferencedFont.TIMES_NEW_ROMAN_REGULAR_FONT))
                .setDocumentFontSize(documentFontSize.getValue())
                .setDocumentPageSize(pageFormats.getOrDefault(documentPageSize.getValue(), PageSize.Default))
                .isPageRotated(documentOrientation.getValue().equals(HORIZONTAL_PAGE_ORIENTATION))
                .setDocumentTopMargin(topDocumentMargin.getValue())
                .setDocumentLeftMargin(leftDocumentMargin.getValue())
                .setDocumentRightMargin(rightDocumentMargin.getValue())
                .setDocumentBottomMargin(bottomDocumentMargin.getValue())
                .build();
        System.out.println(configuration.toString());
    }

    public PdfDocumentConfiguration getConfiguration() {
        return configuration;
    }

}
