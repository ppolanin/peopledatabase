package pl.polanin.domain;

public enum Gender {
    K("K"),
    M("M");

    private String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    Gender() {
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStringGender() {
        return gender;
    }
}
