package pl.polanin.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity()
@Table(name = "Person")
public class Person implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer personId;
    @Column(name = "imie", nullable = false)
    private String firstName;
    @Column(name = "data_urodzenia", nullable = false)
    private LocalDate dateOfBirth;
    @Column(name = "nazwisko", nullable = false)
    private String lastName;
    @Enumerated(EnumType.STRING)
    @Column(name = "plec", nullable = false)
    private Gender gender;
    @Column(name = "obywatelstwo")
    private String nationality;
    @Column(name = "PESEL", nullable = false)
    private String personalNumber;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "kraj", nullable = false)),
            @AttributeOverride(name = "city", column = @Column(name = "miasto", nullable = false)),
            @AttributeOverride(name = "street", column = @Column(name = "ulica", nullable = false)),
            @AttributeOverride(name = "buildingNo", column = @Column(name = "nr_budynku", nullable = false)),
            @AttributeOverride(name = "zipCode", column = @Column(name = "kod_pocztowy", nullable = false))
    })
    private Address address;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "email", column = @Column(name = "email")),
            @AttributeOverride(name = "mobileNumber", column = @Column(name = "telefon_komorkowy", nullable = false)),
            @AttributeOverride(name = "landLine", column = @Column(name = "telefon_stacjonarny"))
    })
    private Contact contact;

    public Person(String firstName, String lastName, LocalDate dateOfBirth, String personalNumber, Address address, Contact contact) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.personalNumber = personalNumber;
        this.address = address;
        this.contact = contact;
    }

    public Person(String firstName, String lastName, LocalDate dateOfBirth, String personalNumber, Address address, Contact contact, Gender gender) {
        this(firstName, lastName, dateOfBirth, personalNumber, address, contact);
        this.gender = gender;
    }

    public Person(String firstName, String lastName, LocalDate dateOfBirth, String personalNumber, Address address, Contact contact, Gender gender, String nationality) {
        this(firstName, lastName, dateOfBirth, personalNumber, address, contact, gender);
        this.nationality = nationality;
    }

    public Person() {
    }


    public int getPersonId() {
        return personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Person person = (Person) object;
        return new EqualsBuilder()
                .append(personalNumber, person.personalNumber)
                .append(firstName, person.firstName)
                .append(lastName, person.lastName)
                .append(dateOfBirth, person.dateOfBirth)
                .append(gender, person.gender)
                .append(nationality, person.nationality)
                .append(address, person.address)
                .append(contact, person.contact)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(personalNumber)
                .append(firstName)
                .append(lastName)
                .append(dateOfBirth)
                .append(gender)
                .append(nationality)
                .append(address)
                .append(contact)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id: ", personId)
                .append("first name: ", firstName)
                .append("last Name: ", lastName)
                .append("date of birth: ", dateOfBirth)
                .append("gender: ", gender)
                .append("nationality: ", nationality)
                .append("address: ", address)
                .append("contact: ", contact).toString();
    }
}
