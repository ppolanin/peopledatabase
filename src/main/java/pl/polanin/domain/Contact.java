package pl.polanin.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Contact implements Serializable {

    String email;
    String mobileNumber;
    String landLine;

    public Contact(String mobileNumber) {
        this(null, mobileNumber, null);
    }

    public Contact(String email, String mobileNumber) {
        this(email, mobileNumber, null);
    }

    public Contact(String email, String mobileNumber, String landLine) {
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.landLine = landLine;
    }

    public Contact() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Contact contact = (Contact) object;
        return new EqualsBuilder()
                .append(email, contact.email)
                .append(mobileNumber, contact.mobileNumber)
                .append(landLine, contact.landLine)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(email)
                .append(mobileNumber)
                .append(landLine)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("email: ", email)
                .append("mobile phone number: ", mobileNumber)
                .append("land line phone number: ", landLine)
                .toString();
    }
}
