package pl.polanin.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Address implements Serializable {

    private String country;
    private String city;
    private String street;
    private String buildingNo;
    private String zipCode;

    public Address(String country, String city, String street, String buildingNo, String zipCode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.buildingNo = buildingNo;
        this.zipCode = zipCode;
    }

    public Address() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(String buildingNo) {
        this.buildingNo = buildingNo;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Address address = (Address) object;
        return new EqualsBuilder()
                .append(country, address.country)
                .append(city, address.city)
                .append(street, address.street)
                .append(buildingNo, address.buildingNo)
                .append(zipCode, address.zipCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(country)
                .append(city)
                .append(street)
                .append(buildingNo)
                .append(zipCode)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("country: ", country)
                .append("city: ", city)
                .append("street: ", street)
                .append("number of building: ", buildingNo)
                .append("ZIP code: ", zipCode)
                .toString();
    }
}
