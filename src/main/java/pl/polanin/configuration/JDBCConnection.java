package pl.polanin.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

    private final DbConfigReader dbConfigReader;

    public JDBCConnection(DbConfigReader dbConfigReader) {
        this.dbConfigReader = dbConfigReader;
    }

    public Connection connectionToDB() throws SQLException {
        return DriverManager.getConnection(dbConfigReader.getUrl(), dbConfigReader.getUser(), dbConfigReader.getPassword());
    }
}
