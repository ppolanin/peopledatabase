package pl.polanin.configuration;

import java.io.IOException;
import java.util.Properties;

public class DbConfigReader {

    private final String url;
    private final String user;
    private final String password;

    private DbConfigReader(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public static DbConfigReader loadDbConfiguration() throws IOException {
        Properties properties = new Properties();
        properties.load(DbConfigReader.class.getClassLoader().getResourceAsStream("META-INFO/dbconfig.properties"));
        return new DbConfigReader(properties.getProperty("dbURL"), properties.getProperty("dbUSER"), properties.getProperty("dbPASSWORD"));
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
