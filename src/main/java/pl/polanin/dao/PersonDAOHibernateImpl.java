package pl.polanin.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.polanin.domain.Person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class PersonDAOHibernateImpl implements PersonDAO {

    private static final SessionFactory sessionFactory;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDAOHibernateImpl.class);

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("META-INFO/hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(registry).buildMetadata();
        sessionFactory = metadata.buildSessionFactory();
    }

    @Override
    public Optional<Person> getPerson(int personId) {
        Session session = sessionFactory.openSession();
        return Optional.ofNullable(session.find(Person.class, personId));
    }

    @Override
    public List<Person> getPeople() {
        Session session = sessionFactory.openSession();
        String sqlQuery = "FROM Person";
        Query<Person> sessionQuery = session.createQuery(sqlQuery, Person.class);
        List<Person> basicPersonalInfo = sessionQuery.getResultList();
        session.close();
        return basicPersonalInfo;
    }

    @Override
    public int addPersonData(Person person) {
        runInTransaction(session -> session.save(person));
        LOGGER.info(person.toString() + " was saved to DB");
        return person.getPersonId();
    }

    @Override
    public int updatePersonData(Person person) {
        runInTransaction(session -> session.update(person));
        return person.getPersonId();
    }

    @Override
    public void removePersonData(Person person) {
        runInTransaction(session -> session.delete(person));
    }

    @Override
    public void removePeople() {
        runInTransaction(session -> {
            List<Person> people = session.createQuery("FROM Person", Person.class).list();
            people.forEach(session::delete);
        });
    }

    @Override
    public boolean isPersonExist(Person person) {
        if (Optional.ofNullable(person).isPresent()) {
            Session session = sessionFactory.openSession();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Person> criteria = criteriaBuilder.createQuery(Person.class);
            Root<Person> root = criteria.from(Person.class);
            Predicate firstNamePredicate = criteriaBuilder.equal(root.get("firstName"), person.getFirstName());
            Predicate lastNamePredicate = criteriaBuilder.equal(root.get("lastName"), person.getLastName());
            Predicate personalNumberPredicate = criteriaBuilder.equal(root.get("personalNumber"), person.getPersonalNumber());
            criteria.select(root)
                    .where(criteriaBuilder.and(firstNamePredicate, lastNamePredicate, personalNumberPredicate));
            Person foundPerson = session.createQuery(criteria).uniqueResult();
            if (Optional.ofNullable(foundPerson).isPresent()) {
                    return true;
            }
            session.close();
        }
        return false;
    }

    private void runInTransaction(Consumer<Session> action) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            action.accept(session);
            transaction.commit();
        } catch (RuntimeException rex) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            LOGGER.error(rex.getMessage(), rex);
        }
        session.close();
    }

}
