package pl.polanin.dao;

import pl.polanin.domain.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {

    List<Person> getPeople();

    int addPersonData(Person person);

    int updatePersonData(Person person);

    void removePersonData(Person person);

    void removePeople();

    boolean isPersonExist(Person person);

    Optional<Person> getPerson(int personId);
}
