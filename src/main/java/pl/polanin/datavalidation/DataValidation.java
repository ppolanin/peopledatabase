package pl.polanin.datavalidation;

import pl.polanin.domain.Gender;
import pl.polanin.support.MonthLaboratoryOfPersonIdNumber;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataValidation {

    private static final List<String> invalidCharacters = Arrays.asList("`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=", "+", "/", "[", "{", "]", "}", "|", "\\", ";", ":", "'", "\"", ",", "<", ".", ">", "?");

    public static boolean containsInvalidWord(String word) {
        if (word != null) {
            for (String c : invalidCharacters) {
                if (word.contains(c)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsInvalidCharacterDespiteDash(String word) {
        char acceptableMark = '-';
        if (word != null) {
            if (word.charAt(0) == acceptableMark || word.charAt(word.length() - 1) == acceptableMark) {
                return true;
            } else {
                char[] letters = word.toCharArray();
                for (char letter : letters) {
                    for (String invalidCharacter : invalidCharacters) {
                        if (invalidCharacter.toCharArray()[0] != acceptableMark) {
                            if (letter == invalidCharacter.toCharArray()[0]) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean isStringLengthValid(String word) {
        if (word != null) {
            return word.length() >= 2 && word.length() <= 255;
        }
        return false;
    }

    public static boolean containsDigits(String word) {
        if (word != null) {
            String digitPattern = ".*[0-9]+.*";
            return Pattern.compile(digitPattern).matcher(word).matches();
        }
        return false;
    }

    public static boolean isNameValid(String name) {
        if (name != null) {
            return isStringLengthValid(name) && !containsDigits(name) && !containsInvalidWord(name);
        }
        return false;
    }

    public static boolean isNameWithDashValid(String name) {
        if (name != null) {
            return isStringLengthValid(name) && !containsDigits(name) && !containsInvalidCharacterDespiteDash(name);
        }
        return false;
    }

    public static boolean isDateValid(String date) {
        if (date != null) {
            String inspectedDate = date.replace(".", "-").replace(" ", "");
            String datePattern = "(\\d\\d)-(\\d\\d)-(\\d\\d\\d\\d)";
            Matcher dateMatcher = Pattern.compile(datePattern).matcher(inspectedDate);
            if (dateMatcher.matches()) {
                try {
                    LocalDate.of(Integer.parseInt(dateMatcher.group(3)), Integer.parseInt(dateMatcher.group(2)), Integer.parseInt(dateMatcher.group(1)));
                    return true;
                } catch (DateTimeException dtpEx) {
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean isGenderValid(String gender) {
        if (gender != null) {
            String inspectedGender = gender.toUpperCase();
            return inspectedGender.equals("K") || inspectedGender.equals("M");
        }
        return false;
    }

    public static boolean isPersonIdNumberValid(String personIdNumber, LocalDate birthDate, Gender gender) {
        int personIdNumberLength = 11;
        MonthLaboratoryOfPersonIdNumber personIdNumberMonthLaboratory = MonthLaboratoryOfPersonIdNumber.getInstance();
        Map<String, MonthLaboratoryOfPersonIdNumber.MonthOfPersonIdNumber> personIdNumberMonths = personIdNumberMonthLaboratory.getMonthsOfPersonIdNumber();

        if (gender != null && birthDate != null && personIdNumber != null && personIdNumber.length() == personIdNumberLength) {
            int birthDay = birthDate.getDayOfMonth();
            int birthMonth = birthDate.getMonth().getValue();
            int birthYear = birthDate.getYear();

            int personIdNumberBirthMonth = 0;
            int personIdNumberBirthYear = 0;
            for (Map.Entry<String, MonthLaboratoryOfPersonIdNumber.MonthOfPersonIdNumber> entry : personIdNumberMonths.entrySet()) {
                String personIdNumberMonthsKey = entry.getKey();
                MonthLaboratoryOfPersonIdNumber.MonthOfPersonIdNumber personIdNumberMonthsValue = entry.getValue();
                if (personIdNumberMonthsKey.equals(personIdNumber.substring(2, 4))) {
                    personIdNumberBirthMonth = personIdNumberMonthsValue.getMonthNumber();
                    personIdNumberBirthYear = Integer.parseInt(personIdNumberMonthsValue.getCenturyPrefix() + personIdNumber.substring(0, 2));
                    break;
                }
            }

            int personIdNumberBirthDay = Integer.parseInt(personIdNumber.substring(4, 6));
            int personIdNumberGenderNumber = Integer.parseInt(personIdNumber.substring(9, 10));
            int personIdNumberControlNumber = Integer.parseInt(personIdNumber.substring(10, 11));
            int calculatedControlNumberOfPersonIdNumber = calculateControlNumberOfPersonIdNumber(personIdNumber);

            String personIdNumberGenderLiteralIndication;
            if (personIdNumberGenderNumber % 2 == 0) {
                personIdNumberGenderLiteralIndication = "K";
            } else {
                personIdNumberGenderLiteralIndication = "M";
            }

            return birthDay == personIdNumberBirthDay && birthMonth == personIdNumberBirthMonth && birthYear == personIdNumberBirthYear && personIdNumberGenderLiteralIndication.equals(gender.getStringGender()) && personIdNumberControlNumber == calculatedControlNumberOfPersonIdNumber;
        }
        return false;
    }

    public static int calculateControlNumberOfPersonIdNumber(String personIdNumber) {
        int referenceNumber = 10;
        int additionResult = 0;
        if (personIdNumber != null && personIdNumber.length() == 11) {
            List<Integer> weightsFunction = Arrays.asList(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);

            int multiplicationResult;
            for (int i = 0; i < personIdNumber.length() - 1; i++) {
                int personIdNumberDigit = Integer.parseInt(String.valueOf(personIdNumber.charAt(i)));
                multiplicationResult = personIdNumberDigit * weightsFunction.get(i);
                if (multiplicationResult > 9 && multiplicationResult < 100) {
                    multiplicationResult = Integer.parseInt(String.valueOf(multiplicationResult).substring(1));
                }
                additionResult += multiplicationResult;
            }
            if (additionResult > 9 && additionResult < 100) {
                additionResult = Integer.parseInt(String.valueOf(additionResult).substring(1));
            }
        }
        return referenceNumber - additionResult;
    }

    public static boolean isAddressElementValid(String addressName) {
        if (addressName != null) {
            return isStringLengthValid(addressName) && !containsDigits(addressName) && !containsInvalidCharacterDespiteDash(addressName);
        }
        return false;
    }

    public static boolean isBuildingNoValid(String buildingNo) {
        if (buildingNo != null) {
            String buildingNoPattern = "^[1-9][0-9]*[a-zA-Z]?/?[1-9]*";
            return Pattern.compile(buildingNoPattern).matcher(buildingNo).matches() && buildingNo.length() <= 15;
        }
        return false;
    }

    public static boolean isZipCodeValid(String zipCode) {
        if (zipCode != null) {
            String zipCodePattern = "\\d{2}-\\d{3}";
            return Pattern.compile(zipCodePattern).matcher(zipCode).matches();
        }
        return false;
    }

    public static boolean isMailValid(String emailAddress) {
        if (emailAddress != null) {
            String mailPattern = ".+@[a-z0-9]+\\.[a-z]{2,}";
            return Pattern.compile(mailPattern).matcher(emailAddress).matches();
        }
        return false;
    }

    public static boolean isMobilePhoneNumberValid(String mobilePhoneNumber) {
        if (mobilePhoneNumber != null) {
            String mobilePhoneNumberPattern = "\\+\\d{11}";
            return Pattern.compile(mobilePhoneNumberPattern).matcher(mobilePhoneNumber).matches();
        }
        return false;
    }

    public static boolean isLandBasedPhoneNumberValid(String landBasedPhoneNumber) {
        if (landBasedPhoneNumber != null) {
            String landBasedPhoneNumberPattern = "\\d{9}";
            return Pattern.compile(landBasedPhoneNumberPattern).matcher(landBasedPhoneNumber).matches();
        }
        return false;
    }


}