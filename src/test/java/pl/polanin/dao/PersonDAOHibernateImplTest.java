package pl.polanin.dao;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.polanin.configuration.DbConfigReader;
import pl.polanin.configuration.JDBCConnection;
import pl.polanin.configuration.TestUtil;
import pl.polanin.domain.Address;
import pl.polanin.domain.Contact;
import pl.polanin.domain.Gender;
import pl.polanin.domain.Person;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class PersonDAOHibernateImplTest {

    private PersonDAO personDAO;
    private JDBCConnection jdbcConnection;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDAOHibernateImplTest.class);

    @BeforeEach
    public void setup() {
        try {
            personDAO = new PersonDAOHibernateImpl();
            jdbcConnection = new JDBCConnection(DbConfigReader.loadDbConfiguration());
            jdbcConnection.connectionToDB();
            TestUtil.setupDB(jdbcConnection);
        } catch (IOException | SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @AfterEach
    public void dropDB() {
        TestUtil.clearDB(jdbcConnection);
    }

    @Test
    public void getPersonTest() {
        //Given
        Address personAddress = new Address("Polska", "Lublin", "Dobra", "31", "20-350");
        Contact personContact = new Contact("irongirl85@o2.pl", "+48999999999", null);
        //When
        Optional<Person> optionalPerson = personDAO.getPerson(9);
        //Then
        assertThat(optionalPerson).isNotEmpty();
        assertThat(optionalPerson).containsInstanceOf(Person.class);
        Condition<Person> expectedId = new Condition<>(p -> p.getPersonId() == 9, "person id");
        Condition<Person> expectedFirstName = new Condition<>(p -> p.getFirstName().equals("Magdalena"), "first name");
        Condition<Person> expectedLastName = new Condition<>(p -> p.getLastName().equals("Storczyk"), "last name");
        Condition<Person> expectedGender = new Condition<>(p -> p.getGender() == Gender.K, "gender");
        Condition<Person> expectedPersonalNumber = new Condition<>(p -> p.getPersonalNumber().equals("85112456183"), "personal number");
        Condition<Person> expectedBirthDate = new Condition<>(p -> p.getDateOfBirth().equals(LocalDate.of(1985, 11, 24)), "birth date");
        Condition<Person> expectedNationality = new Condition<>(p -> p.getNationality().equals("polskie"), "nationality");
        Condition<Person> expectedAddress = new Condition<>(p -> p.getAddress().equals(personAddress), "address");
        Condition<Person> expectedContact = new Condition<>(p -> p.getContact().equals(personContact), "contact");
        assertThat(optionalPerson).hasValueSatisfying(expectedId);
        assertThat(optionalPerson).hasValueSatisfying(expectedFirstName);
        assertThat(optionalPerson).hasValueSatisfying(expectedLastName);
        assertThat(optionalPerson).hasValueSatisfying(expectedGender);
        assertThat(optionalPerson).hasValueSatisfying(expectedPersonalNumber);
        assertThat(optionalPerson).hasValueSatisfying(expectedBirthDate);
        assertThat(optionalPerson).hasValueSatisfying(expectedNationality);
        assertThat(optionalPerson).hasValueSatisfying(expectedAddress);
        assertThat(optionalPerson).hasValueSatisfying(expectedContact);
    }

    @Test
    public void getPeopleTest() {
        //Given
        List<Person> people = personDAO.getPeople();
        //When
        int expectedListSize = 11;
        //Then
        assertThat(people.isEmpty()).isFalse();
        assertThat(people.size()).isEqualTo(expectedListSize);
    }

    @Test
    public void addPersonDataTest() {
        //Given
        Address address = new Address("Polska", "Katowice", "Kotlarza", "45a", "40-145");
        Contact contact = new Contact("sugarman80@o2.pl", "+48665023457", "322562514");
        LocalDate birthDate = LocalDate.of(1980, 4, 3);
        Person person = new Person("Marcin", "Klimek", birthDate, "80040356279", address, contact, Gender.M, "polskie");
        //When
        int personId = 12;
        int testedPersonId = personDAO.addPersonData(person);
        Person expectedPerson = personDAO.getPerson(12).isPresent() ? personDAO.getPerson(12).get() : null;
        //Then
        assertThat(testedPersonId).isEqualTo(personId);
        assertThat(expectedPerson).isNotNull();
        assertThat(expectedPerson.getFirstName()).isEqualTo("Marcin");
        assertThat(expectedPerson.getLastName()).isEqualTo("Klimek");
        assertThat(expectedPerson.getPersonalNumber()).isEqualTo("80040356279");
    }

    @Test
    public void removePersonDataTest() {
        //Given
        Person person = personDAO.getPerson(9).get();
        //When
        personDAO.removePersonData(person);
        List<Person> people = personDAO.getPeople();
        //Then
        assertThat(people).isNotEmpty();
        assertThat(people).hasSize(10);
        assertThat(people).doesNotContain(person);
    }

    @Test
    public void removePeopleTest() {
        //When
        personDAO.removePeople();
        List<Person> people = personDAO.getPeople();
        //Then
        assertThat(people).isNotNull();
        assertThat(people).isEmpty();
    }

    @Test
    public void isPersonExistTest() {
        //Given
        Address address = new Address("Polska", "Łódź", "Piotrowska", "58", "90-105");
        Contact contact = new Contact("jnowak@gmail.com", "+48111111111");
        Person existPerson = new Person("Jan", "Nowak", LocalDate.of(1976, 10, 12), "76101225699", address, contact, Gender.M, "polskie");

        Address newAddress = new Address("Polska", "Zielona Góra", "Akademicka", "58", "65-240");
        Contact newContact = new Contact("+48222222222");
        Person newPerson = new Person("Marta", "Fiołek", LocalDate.of(1999, 9, 2), "99090243187", newAddress, newContact, Gender.K, "polskie");

        Person existPersonWithNoAddressAndContact = new Person("Jan", "Nowak", LocalDate.of(1976, 10, 12), "76101225699", null, null, Gender.M, "polskie");

        //When
        boolean searchingResultForExistPerson = personDAO.isPersonExist(existPerson);
        boolean searchingResultForNewPerson = personDAO.isPersonExist(newPerson);
        boolean searchingResultForExistPersonWithNoAddressAndContact = personDAO.isPersonExist(existPersonWithNoAddressAndContact);
        //Then
        assertThat(searchingResultForExistPerson).isTrue();
        assertThat(searchingResultForNewPerson).isFalse();
        assertThat(searchingResultForExistPersonWithNoAddressAndContact).isTrue();
    }

    @Test
    public void updatePersonDataTest() {
        //Given
        String newPersonLastName = "Topola";
        String newPersonLandLinePhoneNumber = "111111111";
        String newPersonMobilePhoneNumber = "+48000000000";
        String newPersonCity = "Bytom";
        String newPersonStreet = "Stolarzowicka";
        String newPersonBuildingNo = "125";
        String newPersonZipCode = "41-908";
        Person person = personDAO.getPeople().get(2);
        person.setLastName(newPersonLastName);
        person.getAddress().setCity(newPersonCity);
        person.getAddress().setStreet(newPersonStreet);
        person.getAddress().setBuildingNo(newPersonBuildingNo);
        person.getAddress().setZipCode(newPersonZipCode);
        person.getContact().setLandLine(newPersonLandLinePhoneNumber);
        person.getContact().setMobileNumber(newPersonMobilePhoneNumber);
        //When
        int personIdAfterUpdate = personDAO.updatePersonData(person);
        Person personAfterDataUpdate = personDAO.getPeople().get(2);
        //Then
        assertThat(personIdAfterUpdate).isEqualTo(3);
        assertThat(personAfterDataUpdate.getPersonId()).isEqualTo(3);
        assertThat(personAfterDataUpdate.getLastName()).isEqualTo(newPersonLastName);
        assertThat(personAfterDataUpdate.getAddress().getCity()).isEqualTo(newPersonCity);
        assertThat(personAfterDataUpdate.getAddress().getStreet()).isEqualTo(newPersonStreet);
        assertThat(personAfterDataUpdate.getAddress().getBuildingNo()).isEqualTo(newPersonBuildingNo);
        assertThat(personAfterDataUpdate.getAddress().getZipCode()).isEqualTo(newPersonZipCode);
        assertThat(personAfterDataUpdate.getContact().getLandLine()).isEqualTo(newPersonLandLinePhoneNumber);
        assertThat(personAfterDataUpdate.getContact().getMobileNumber()).isEqualTo(newPersonMobilePhoneNumber);
    }

}
