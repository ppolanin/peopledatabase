package pl.polanin.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

public class TestUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(TestUtil.class);

    public static void setupDB(JDBCConnection jdbcConnection) {
        dbExecution(jdbcConnection, "setupPersonDBTest.sql");
    }

    public static void clearDB(JDBCConnection jdbcConnection) {
        dbExecution(jdbcConnection, "truncatePersonDBTest.sql");
    }

    private static void dbExecution(JDBCConnection jdbcConnection, String sql) {
        InputStream inputStream = TestUtil.class.getClassLoader().getResourceAsStream(sql);
        String sqlStatement = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
        try (Connection connection = jdbcConnection.connectionToDB();
             Statement statement = connection.createStatement()) {
            LOGGER.info("SQL statement: " + sqlStatement);
            statement.executeUpdate(sqlStatement);
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
}
