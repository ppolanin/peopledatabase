package pl.polanin.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JDBCConnectionTest {

    private JDBCConnection jdbcConnection;
    private DbConfigReader dbConfigReader;
    private static final Logger LOGGER = LoggerFactory.getLogger(JDBCConnectionTest.class);

    @BeforeEach
    public void setup() {
        try {
            dbConfigReader = DbConfigReader.loadDbConfiguration();
            jdbcConnection = new JDBCConnection(dbConfigReader);
            LOGGER.info("Connection is ready");
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Test
    public void runTest() {
        LOGGER.info("Test done");
    }
}
