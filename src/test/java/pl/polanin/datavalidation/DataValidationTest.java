package pl.polanin.datavalidation;

import org.junit.jupiter.api.Test;
import pl.polanin.domain.Gender;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class DataValidationTest {

    private final List<Character> invalidCharacters = Arrays.asList('`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '/', '[', '{', ']', '}', '|', '\\', ';', ':', '\'', '\"', ',', '<', '.', '>', '?');
    private final List<String> referenceNames = Arrays.asList("Peter", "John", "Ann", "Chris", "Mark", "Luke", "Jessica", "Mary", "Jane", "Cathy", "Evelyn", "Martha", "Abel", "Betty", "Christina", "Diana", "Frank", "George", "Hilary", "Isabelle", "Louis", "Nelly", "Patrick", "Robert", "Steven", "Sasha", "Thomas", "Joshua", "Clary", "Samantha", "Salma", "Miriam");

    @Test
    public void containsInvalidWordTest() {
        //Given
        List<String> testedNames = new ArrayList<>();
        int counter = 0;
        for (Character s : invalidCharacters) {
            counter++;
            if (counter % 2 == 0) {
                testedNames.add(referenceNames.get(counter - 1) + s);
            } else {
                testedNames.add(s + referenceNames.get(counter - 1));
            }
        }
        //Then
        assertThat(false).isFalse();
        for (String rn : testedNames) {
            assertThat(DataValidation.containsInvalidWord(rn)).isTrue();
        }
    }

    @Test
    public void isStringLengthValidTest() {
        //Given
        String testedName1 = "P";
        String testedName2 = referenceNames.get(5);
        String testedName3 = "Al";
        String testedName4 = "";
        String testedName5 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nulla erat, pellentesque a pulvinar ut, porta sed nisi. Nunc in ullamcorper nibh, at commodo orci. Nunc augue velit, convallis et orci vel, condimentum consectetur quam. Fusce eros orci, ullamcorper a purus tincidunt, lobortis egestas massa. Aenean suscipit vehicula sem. Vestibulum velit mi, gravida nec aliquam vitae, sodales vitae velit. Ut elementum luctus pretium. Duis ut rutrum libero.\n" +
                "Aliquam vitae varius sapien, non dapibus arcu. Pellentesque eget odio id lorem pulvinar faucibus consequat a risus. Aliquam imperdiet dignissim mi. Pellentesque consequat bibendum nisi quis ultricies. Phasellus tellus elit, pulvinar sed turpis at, gravida aliquam libero. Aenean ultricies dapibus tempor. Donec fringilla malesuada velit eu pulvinar. Aliquam erat volutpat. Praesent ac orci cursus, scelerisque nisi a, auctor elit. Maecenas pharetra condimentum neque, sit amet elementum velit mattis in. Praesent ultricies massa egestas interdum molestie. Mauris maximus laoreet orci, quis imperdiet purus congue sodales. Fusce vel scelerisque mi, volutpat elementum ipsum.\n" +
                "Pellentesque quam orci, efficitur vitae consequat vitae, maximus eget mauris. Praesent consectetur diam sed interdum euismod. Aliquam erat volutpat. Curabitur quis leo congue, viverra velit eu, gravida nibh. Duis malesuada tempus nibh. Pellentesque vel mi in lectus ullamcorper venenatis. Donec aliquet lobortis sollicitudin. Aliquam ut ante euismod, placerat velit nec, volutpat nisi. Cras quis euismod nisl, nec cursus ante. Mauris a nibh dolor. Etiam malesuada ligula eget tristique vulputate.\n" +
                "Nulla nec facilisis ex. Phasellus vel lacus gravida, lobortis quam vel, fermentum magna. Pellentesque nec nibh rutrum, maximus dui et, eleifend nulla. Pellentesque facilisis, lectus laoreet venenatis varius, massa erat convallis mi, non consectetur nisi mauris non ipsum. Nulla a egestas augue, vitae euismod nulla. Sed nisi metus, feugiat ac lacus vitae, tempor fringilla odio. Praesent libero ligula, rhoncus eget iaculis quis, scelerisque id ante. Mauris efficitur sapien neque, eu pharetra quam blandit.";
        //When
        boolean testedResult1 = DataValidation.isStringLengthValid(testedName1);
        boolean testedResult2 = DataValidation.isStringLengthValid(testedName2);
        boolean testedResult3 = DataValidation.isStringLengthValid(testedName3);
        boolean testedResult4 = DataValidation.isStringLengthValid(testedName4);
        boolean testedResult5 = DataValidation.isStringLengthValid(testedName5);
        //Then
        assertThat(testedResult1).isFalse();
        assertThat(testedResult2).isTrue();
        assertThat(testedResult3).isTrue();
        assertThat(testedResult4).isFalse();
        assertThat(testedResult5).isFalse();
    }

    @Test
    public void containsDigitsTest() {
        //Given
        String var1 = "123";
        String var2 = "football5";
        String var3 = "25.5monkey";
        String var4 = "hot-3-dog";
        String var5 = "cat";
        //When
        boolean testedResultForVar1 = DataValidation.containsDigits(var1);
        boolean testedResultForVar2 = DataValidation.containsDigits(var2);
        boolean testedResultForVar3 = DataValidation.containsDigits(var3);
        boolean testedResultForVar4 = DataValidation.containsDigits(var4);
        boolean testedResultForVar5 = DataValidation.containsDigits(var5);
        //Then
        assertThat(testedResultForVar1).isTrue();
        assertThat(testedResultForVar2).isTrue();
        assertThat(testedResultForVar3).isTrue();
        assertThat(testedResultForVar4).isTrue();
        assertThat(testedResultForVar5).isFalse();
    }

    @Test
    public void isNameValidTest() {
        //Given
        String firstName = referenceNames.get(0);
        String secondName = "N";
        String thirdName = referenceNames.get(1) + invalidCharacters.get(0);
        String intNumber = "123";
        String doubleNumber = "-25.5";
        String nickName = "john34";
        //When
        boolean testedFirstName = DataValidation.isNameValid(firstName);
        boolean testedSecondName = DataValidation.isNameValid(secondName);
        boolean testedThirdName = DataValidation.isNameValid(thirdName);
        boolean testedIntNumber = DataValidation.isNameValid(intNumber);
        boolean testedDoubleNumber = DataValidation.isNameValid(doubleNumber);
        boolean testedNickName = DataValidation.isNameValid(nickName);
        //Then
        assertThat(testedFirstName).isTrue();
        assertThat(testedSecondName).isFalse();
        assertThat(testedThirdName).isFalse();
        assertThat(testedIntNumber).isFalse();
        assertThat(testedDoubleNumber).isFalse();
        assertThat(testedNickName).isFalse();
    }

    @Test
    public void isDateValidTest() {
        //Given
        String date1 = "25.05.1982";
        String date2 = "15-04-1978";
        String date3 = "12345";
        String date4 = "253-10-1988";
        String date5 = "13-110-1945";
        String date6 = "01-03-21945";
        String date7 = "something";
        String date8 = "31.09.1965";
        String date9 = "1925.5.1";
        String date10 = " 15.  05.       1925 ";
        //When
        boolean dateResult1 = DataValidation.isDateValid(date1);
        boolean dateResult2 = DataValidation.isDateValid(date2);
        boolean dateResult3 = DataValidation.isDateValid(date3);
        boolean dateResult4 = DataValidation.isDateValid(date4);
        boolean dateResult5 = DataValidation.isDateValid(date5);
        boolean dateResult6 = DataValidation.isDateValid(date6);
        boolean dateResult7 = DataValidation.isDateValid(date7);
        boolean dateResult8 = DataValidation.isDateValid(date8);
        boolean dateResult9 = DataValidation.isDateValid(date9);
        boolean dateResult10 = DataValidation.isDateValid(date10);
        //Then
        assertThat(dateResult1).isTrue();
        assertThat(dateResult2).isTrue();
        assertThat(dateResult3).isFalse();
        assertThat(dateResult4).isFalse();
        assertThat(dateResult5).isFalse();
        assertThat(dateResult6).isFalse();
        assertThat(dateResult7).isFalse();
        assertThat(dateResult8).isFalse();
        assertThat(dateResult9).isFalse();
        assertThat(dateResult10).isTrue();
    }

    @Test
    public void isGenderValidTest() {
        //Given
        String maleWithLowerCase = "m";
        String maleWithUpperCase = "M";
        String femaleWithLowerCase = "k";
        String femaleWithUpperCase = "K";
        String wordWithLetterK = "work";
        String wordWithLetterM = "marathon";
        //When
        boolean maleWithLowerCaseResult = DataValidation.isGenderValid(maleWithLowerCase);
        boolean maleWithUpperCaseResult = DataValidation.isGenderValid(maleWithUpperCase);
        boolean femaleWithLowerCaseResult = DataValidation.isGenderValid(femaleWithLowerCase);
        boolean femaleWithUpperCaseResult = DataValidation.isGenderValid(femaleWithUpperCase);
        boolean wordWithLetterKResult = DataValidation.isGenderValid(wordWithLetterK);
        boolean wordWithLetterMResult = DataValidation.isGenderValid(wordWithLetterM);
        //Then
        assertThat(maleWithLowerCaseResult).isTrue();
        assertThat(maleWithUpperCaseResult).isTrue();
        assertThat(femaleWithLowerCaseResult).isTrue();
        assertThat(femaleWithUpperCaseResult).isTrue();
        assertThat(wordWithLetterKResult).isFalse();
        assertThat(wordWithLetterMResult).isFalse();
    }

    @Test
    public void calculateControlNumberForValidPersonIdNumber() {
        //Given
        String personIdNumber = "82031720553";
        int expectedControlNumber = 3;
        //When
        int testedControlNumber = DataValidation.calculateControlNumberOfPersonIdNumber(personIdNumber);
        //Then
        assertThat(testedControlNumber).isEqualTo(expectedControlNumber);

    }

    @Test
    public void returnTrueWhenWomanWasBornInNineteenthCentury() {
        //Given
        String personIdNumber = "45873005127";
        LocalDate birthDate = LocalDate.of(1845, 7, 30);
        Gender gender = Gender.K;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenManWasBornInNineteenthCentury() {
        //Given
        String personIdNumber = "00810113893";
        LocalDate birthDate = LocalDate.of(1800, 1, 1);
        Gender gender = Gender.M;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenManWasBornInTwentiethCentury() {
        //Given
        String correctPESEL = "82031720553";
        LocalDate correctBirthDate = LocalDate.of(1982, 3, 17);
        Gender correctGender = Gender.M;
        //When
        boolean personIdNumberValidResult = DataValidation.isPersonIdNumberValid(correctPESEL, correctBirthDate, correctGender);
        //Then
        assertThat(personIdNumberValidResult).isTrue();
    }

    @Test
    public void returnTrueWhenWomanWasBornInTwentiethCentury() {
        //Given
        String correctPESEL = "99020673989";
        LocalDate correctBirthDate = LocalDate.of(1999, 2, 6);
        Gender correctGender = Gender.K;
        //When
        boolean personIdNumberValidResult = DataValidation.isPersonIdNumberValid(correctPESEL, correctBirthDate, correctGender);
        //Then
        assertThat(personIdNumberValidResult).isTrue();
    }

    @Test
    public void returnTrueWhenWomanWasBornInTwentyFirstCentury() {
        //Given
        String personIdNumber = "00251525365";
        LocalDate birthDate = LocalDate.of(2000, 5, 15);
        Gender gender = Gender.K;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenManWasBornInTwentyFirstCentury() {
        //Given
        String personIdNumber = "99321815619";
        LocalDate birthDate = LocalDate.of(2099, 12, 18);
        Gender gender = Gender.M;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenWomanWasBornInTwentySecondCentury() {
        //Given
        String personIdNumber = "83460825145";
        LocalDate birthDate = LocalDate.of(2183, 6, 8);
        Gender gender = Gender.K;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenManWasBornInTwentySecondCentury() {
        //Given
        String personIdNumber = "01440728136";
        LocalDate birthDate = LocalDate.of(2101, 4, 7);
        Gender gender = Gender.M;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenWomanWasBornInTwentyThirdCentury() {
        //Given
        String personIdNumber = "51683133981";
        LocalDate birthDate = LocalDate.of(2251, 8, 31);
        Gender gender = Gender.K;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void returnTrueWhenManWasBornInTwentyThirdCentury() {
        //Given
        String personIdNumber = "98692274179";
        LocalDate birthDate = LocalDate.of(2298, 9, 22);
        Gender gender = Gender.M;
        //When
        boolean personIdNumberValid = DataValidation.isPersonIdNumberValid(personIdNumber, birthDate, gender);
        //Then
        assertThat(personIdNumberValid).isTrue();
    }

    @Test
    public void containsInvalidCharacterForAddressForCityName() {
        //Given
        String cityName1 = "-Bielsko-Biała";
        String cityName2 = "Bielsko-Biała-";
        String cityName3 = "-Bielsko-Biała-";
        String cityName4 = "Busko-Zdrój";
        String cityName5 = "!Busko-Zdrój";
        String cityName6 = "Busko-Zdrój?";
        String cityName7 = "Czechowice!Dziedzice";
        //When
        boolean resultForCityName1 = DataValidation.containsInvalidCharacterDespiteDash(cityName1);
        boolean resultForCityName2 = DataValidation.containsInvalidCharacterDespiteDash(cityName2);
        boolean resultForCityName3 = DataValidation.containsInvalidCharacterDespiteDash(cityName3);
        boolean resultForCityName4 = DataValidation.containsInvalidCharacterDespiteDash(cityName4);
        boolean resultForCityName5 = DataValidation.containsInvalidCharacterDespiteDash(cityName5);
        boolean resultForCityName6 = DataValidation.containsInvalidCharacterDespiteDash(cityName6);
        boolean resultForCityName7 = DataValidation.containsInvalidCharacterDespiteDash(cityName7);
        //Then
        assertThat(resultForCityName1).isTrue();
        assertThat(resultForCityName2).isTrue();
        assertThat(resultForCityName3).isTrue();
        assertThat(resultForCityName4).isFalse();
        assertThat(resultForCityName5).isTrue();
        assertThat(resultForCityName6).isTrue();
        assertThat(resultForCityName7).isTrue();
    }

    @Test
    public void isZipCodeValidTest() {
        //Given
        String zipCode1 = "40-145";
        String zipCode2 = "123-123";
        String zipCode3 = "0123-123";
        String zipCode4 = "123=123";
        String zipCode5 = "1-12";
        //Then
        assertThat(DataValidation.isZipCodeValid(zipCode1)).isTrue();
        assertThat(DataValidation.isZipCodeValid(zipCode2)).isFalse();
        assertThat(DataValidation.isZipCodeValid(zipCode3)).isFalse();
        assertThat(DataValidation.isZipCodeValid(zipCode4)).isFalse();
        assertThat(DataValidation.isZipCodeValid(zipCode5)).isFalse();
    }


    @Test
    public void isMailValidTest() {
        //Given
        String testedEmail1 = "fernando_rodriguez@gmail.com";
        String testedEmail2 = "sloneczko1988@o2.pl";
        String testedEmail3 = "@yahoo.com";
        String testedEmail4 = "Viking.Warrior65@hotmailcom";

        //Then
        assertThat(DataValidation.isMailValid(testedEmail1)).isTrue();
        assertThat(DataValidation.isMailValid(testedEmail2)).isTrue();
        assertThat(DataValidation.isMailValid(testedEmail3)).isFalse();
        assertThat(DataValidation.isMailValid(testedEmail4)).isFalse();
    }

    @Test
    public void isMobilePhoneNumberValidTest() {
        //Given
        String testedMobileNumber1 = "+48666666666";
        String testedMobileNumber2 = "48666666666";
        String testedMobileNumber3 = "666666666";
        String testedMobileNumber4 = "6666";
        String testedMobileNumber5 = "+48 666 666 666";

        //Then
        assertThat(DataValidation.isMobilePhoneNumberValid(testedMobileNumber1)).isTrue();
        assertThat(DataValidation.isMobilePhoneNumberValid(testedMobileNumber2)).isFalse();
        assertThat(DataValidation.isMobilePhoneNumberValid(testedMobileNumber3)).isFalse();
        assertThat(DataValidation.isMobilePhoneNumberValid(testedMobileNumber4)).isFalse();
        assertThat(DataValidation.isMobilePhoneNumberValid(testedMobileNumber5)).isFalse();
    }

    @Test
    public void isLandBasedPhoneNumberValid() {
        //Given
        String testedLandBasedPhoneNumber1 = "322594587";
        String testedLandBasedPhoneNumber2 = "1234567";
        String testedLandBasedPhoneNumber3 = "123 45 67";
        String testedLandBasedPhoneNumber4 = "0123456789";
        //Then
        assertThat(DataValidation.isLandBasedPhoneNumberValid(testedLandBasedPhoneNumber1)).isTrue();
        assertThat(DataValidation.isLandBasedPhoneNumberValid(testedLandBasedPhoneNumber2)).isFalse();
        assertThat(DataValidation.isLandBasedPhoneNumberValid(testedLandBasedPhoneNumber3)).isFalse();
        assertThat(DataValidation.isLandBasedPhoneNumberValid(testedLandBasedPhoneNumber4)).isFalse();
    }

    @Test
    public void isBuildingNoValid() {
        //Given
        String testedBuildingNo1 = "0";
        String testedBuildingNo2 = "1";
        String testedBuildingNo3 = "01";
        String testedBuildingNo4 = "103";
        String testedBuildingNo5 = "5A";
        String testedBuildingNo6 = "48c";
        String testedBuildingNo7 = "12/5";
        String testedBuildingNo8 = "0123A";
        String testedBuildingNo9 = "123A\\a";
        String testedBuildingNo10 = "123a//5";
        String testedBuildingNo11 = "1?";
        String testedBuildingNo12 = "/";
        String testedBuildingNo13 = "!89d";
        String testedBuildingNo14 = "123aA";
        String testedBuildingNo15 = "01234567890123456789";
        //Then
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo1)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo2)).isTrue();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo3)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo4)).isTrue();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo5)).isTrue();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo6)).isTrue();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo7)).isTrue();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo8)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo9)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo10)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo11)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo12)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo13)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo14)).isFalse();
        assertThat(DataValidation.isBuildingNoValid(testedBuildingNo15)).isFalse();
    }

}
