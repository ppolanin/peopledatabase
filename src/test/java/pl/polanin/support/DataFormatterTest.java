package pl.polanin.support;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DataFormatterTest {

    @Test
    public void clearGapsTest() {
        //Given
        String word1 = "   John ";
        String word2 = "    Jo  hn    ";
        String word3 = "";
        //When
        String formattedWord1 = DataFormatter.clearGaps(word1);
        String formattedWord2 = DataFormatter.clearGaps(word2);
        String formattedWord3 = DataFormatter.clearGaps(word3);
        //Then
        assertThat(formattedWord1).isEqualTo("John");
        assertThat(formattedWord2).isEqualTo("John");
        assertThat(formattedWord3).isEqualTo("");
    }

    @Test
    public void convertToLowerCase() {
        //Given
        String word1 = "PINEAPPLE";
        String word2 = "Mango";
        String word3 = "";
        //When
        String convertedWord1 = DataFormatter.convertToLowerCase(word1);
        String convertedWord2 = DataFormatter.convertToLowerCase(word2);
        String convertedWord3 = DataFormatter.convertToLowerCase(word3);
        //Then
        assertThat(convertedWord1).isEqualTo("pineapple");
        assertThat(convertedWord2).isEqualTo("mango");
        assertThat(convertedWord3).isEqualTo("");
    }

    @Test
    public void convertToIndividualNameForWord() {
        //Given
        String word1 = "marcus";
        String word2 = "tIGER";
        String word3 = "SPAIN";
        String word4 = "";
        //When
        String convertedWord1 = DataFormatter.convertToIndividualName(word1);
        String convertedWord2 = DataFormatter.convertToIndividualName(word2);
        String convertedWord3 = DataFormatter.convertToIndividualName(word3);
        String convertedWord4 = DataFormatter.convertToIndividualName(word4);
        //Then
        assertThat(convertedWord1).isEqualTo("Marcus");
        assertThat(convertedWord2).isEqualTo("Tiger");
        assertThat(convertedWord3).isEqualTo("Spain");
        assertThat(convertedWord4).isNull();
    }

    @Test
    public void convertToIndividualNameForWords() {
        //Given
        String words1 = "stany zjednoczone";
        String words2 = "tRINIDAD I tobago";
        String words3 = "zjednoczone emiraty arabskie";
        String words4 = "   BOŚNIA i HERCEGOWINA  ";
        String words5 = "   czechowice - dziedzice  ";
        String words6 = "   kOWALSKA- NOWAK  ";
        String words7 = "   kowalska -Nowak  ";
        String words8 = "   kowalska -nOWAK  ";
        //When
        String convertedWords1 = DataFormatter.convertToIndividualName(words1);
        String convertedWords2 = DataFormatter.convertToIndividualName(words2);
        String convertedWords3 = DataFormatter.convertToIndividualName(words3);
        String convertedWords4 = DataFormatter.convertToIndividualName(words4);
        String convertedWords5 = DataFormatter.convertToIndividualName(words5);
        String convertedWords6 = DataFormatter.convertToIndividualName(words6);
        String convertedWords7 = DataFormatter.convertToIndividualName(words7);
        String convertedWords8 = DataFormatter.convertToIndividualName(words8);
        //Then
        assertThat(convertedWords1).isEqualTo("Stany Zjednoczone");
        assertThat(convertedWords2).isEqualTo("Trinidad i Tobago");
        assertThat(convertedWords3).isEqualTo("Zjednoczone Emiraty Arabskie");
        assertThat(convertedWords4).isEqualTo("Bośnia i Hercegowina");
        assertThat(convertedWords5).isEqualTo("Czechowice - Dziedzice");
        assertThat(convertedWords6).isEqualTo("Kowalska- Nowak");
        assertThat(convertedWords7).isEqualTo("Kowalska -Nowak");
        assertThat(convertedWords8).isEqualTo("Kowalska -Nowak");
    }
}
