package pl.polanin.support;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

public class DataConverterTest {

    @Test
    public void convertDateToSQLDateFormatWhenDateIsValid() {
        //Given
        String validTextDate1 = "10-12-2019";
        LocalDate expectedDate1 = LocalDate.parse(validTextDate1, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        String validTextDate2 = "2015-06-10";
        LocalDate expectedDate2 = LocalDate.parse(validTextDate2, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String validTextDate3 = "03.01.1955";
        LocalDate expectedDate3 = LocalDate.parse(validTextDate3, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        String validTextDate4 = "1815.09.30";
        LocalDate expectedDate4 = LocalDate.parse(validTextDate4, DateTimeFormatter.ofPattern("yyyy.MM.dd"));
        //When
        LocalDate testedValidDate1 = DataConverter.convertDateToSQLDateFormat(validTextDate1);
        LocalDate testedValidDate2 = DataConverter.convertDateToSQLDateFormat(validTextDate2);
        LocalDate testedValidDate3 = DataConverter.convertDateToSQLDateFormat(validTextDate3);
        LocalDate testedValidDate4 = DataConverter.convertDateToSQLDateFormat(validTextDate4);
        //Then
        assertThat(testedValidDate1).isEqualTo(expectedDate1);
        assertThat(testedValidDate2).isEqualTo(expectedDate2);
        assertThat(testedValidDate3).isEqualTo(expectedDate3);
        assertThat(testedValidDate4).isEqualTo(expectedDate4);
    }

    @Test
    public void convertDateToSQLDateFormatWhenDateIsInvalidOrNull() {
        //Given
        String invalidTextDate1 = "08-09-19";
        String invalidTextDate2 = "15.02-184";
        //When
        LocalDate testedInvalidTextDate1 = DataConverter.convertDateToSQLDateFormat(invalidTextDate1);
        LocalDate testedInvalidTextDate2 = DataConverter.convertDateToSQLDateFormat(invalidTextDate2);
        //Then
        assertThat(testedInvalidTextDate1).isNull();
        assertThat(testedInvalidTextDate2).isNull();
    }

    @Test
    public void convertDateToSQLDateFormatExceptionTest() {
        //Given
        String invalidTextDate3 = "31.09.1929";
        //Then
        try {
            DataConverter.convertDateToSQLDateFormat(invalidTextDate3);
        } catch (Exception ex) {
            assertThat(ex).isExactlyInstanceOf(DateTimeException.class);
        }
    }

    @Test
    public void convertSQLDateToDotDateFormat() {
        //Given
        String date1 = "1988-12-31";
        String date2 = "aaaa-bb-cc";
        String date3 = "2005.06.15";
        //When
        String date1AfterConversion = DataConverter.convertSQLDateToDotDateFormat(date1);
        String date2AfterConversion = DataConverter.convertSQLDateToDotDateFormat(date2);
        String date3AfterConversion = DataConverter.convertSQLDateToDotDateFormat(date3);
        //Then
        assertThat(date1AfterConversion).isEqualTo("31.12.1988");
        assertThat(date2AfterConversion).isNull();
        assertThat(date3AfterConversion).isEqualTo("15.06.2005");
    }


}